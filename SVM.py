import numpy as np
from sklearn import svm
from sklearn.svm import SVC
from sklearn import metrics
from sklearn.multiclass import OneVsRestClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.neighbors import KNeighborsClassifier
from sklearn.linear_model import LogisticRegression, RidgeClassifier, SGDClassifier

def doSVMmagic(X, y):
    #Train the model using the training sets
    clf = svm.SVC(C=1.0,kernel="rbf",cache_size=4096,probability=True,class_weight='balanced').fit(X, y)
    return clf

#random forest
def doRFmagic(X, y):
    clf = RandomForestClassifier(n_estimators=32,n_jobs=8,class_weight='balanced').fit(X, y)
    return clf

#Nearest Neighbors
def doNNmagic(X, y):
    clf = KNeighborsClassifier(n_jobs=4).fit(X, y)
    return clf

#LinReg
def doLinRegMagic(X, y):
    clf = LogisticRegression(max_iter=1024,penalty='l2').fit(X, y)#class_weight="balanced" worse
    return clf

#LinReg
#does not have predict_proba()
def doRidgeMagic(X, y):
    clf = RidgeClassifier(max_iter=1024).fit(X, y)
    return clf

#SGD classifier
def doSGDmagic(X, y):
    clf = SGDClassifier(loss="log").fit(X, y)
    return clf


def SVM_parallel(MyListNow):
            abundances_now = MyListNow[0]
            data = MyListNow[1]
            classes_train = MyListNow[2]

            abundances = abundances_now[0]
            print(abundances)
            data_now = data[:,abundances]
            clf = doSVMmagic(data_now,classes_train)
#            clf = doRFmagic(data_now,classes_train)
#            clf = doNNmagic(data_now,classes_train)
#            clf = doLinRegMagic(data_now,classes_train)
#            clf = doRidgeMagic(data_now,classes_train)
#            clf = doSGDmagic(data_now,classes_train)
            print("Done with ",abundances)
            return [abundances,clf]
