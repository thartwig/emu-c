import numpy as np
from parameters import up_to_elem, ratios, ElemSaved
from parameters import N_ratios, NSNe, EMPset, expo, N_rand, Ndim
from matplotlib import pyplot as plt
from scipy.interpolate import CubicSpline
from convert_Ishigaki import EMP_miho
from convert_Saga import EMP_saga
from convert_user import EMP_user
from AbundInRange import InRange
from SVM_utilities import get_prob, MyPrediction
from GetUncertainty import doBootstrap

def EvalEMP_SVM(list_abundances,SVM_list,f_mono):

    #plt.ioff()    
    if (EMPset == "Miho18"):
        mask_array, EMP_obs, N_EMP, names, FeH = EMP_miho()
        print("Miho18: Number of EMP stars:",N_EMP)
        reference = np.full(N_EMP,"Ishigaki18")
#        for i in range(N_EMP):
#            print(names[i],EMP_obs[i,:])
    elif(EMPset == "Saga19"):
        mask_array, EMP_obs, N_EMP, names, FeH, BaFe, EuFe = EMP_saga()
        print("Saga19: Number of EMP stars:",N_EMP)
        reference = np.full(N_EMP,"Saga")
#        for i in range(N_EMP):
#            print(names[i],EMP_obs[i,:])
    elif(EMPset == "MihoSaga"):#combine two sets
        mask_array1, EMP_obs1, N_EMP1, names1, FeH1, BaFe1, EuFe1 = EMP_saga()
        mask_array2, EMP_obs2, N_EMP2, names2, FeH2 = EMP_miho()
        mask_array, EMP_obs, names, FeH, BaFe, EuFe = mask_array2, EMP_obs2, names2, FeH2, np.full(N_EMP2,-9.99), np.full(N_EMP2,-9.99)
        reference = np.full(N_EMP2,"Ishigaki18")
        #use Miho's as default

        for i, name1 in enumerate(names1):#saga:
            Found = False
            for j, name2 in enumerate(names2):#miho
                if(name1 == name2):
#                    print("use from Mihos data:",name1)
                    Found = True
                    BaFe[j] = BaFe1[i]#overwrite because not in Miho's data
                    EuFe[j] = EuFe1[i]#overwrite
                    break
            if not Found:
                mask_array = np.vstack((mask_array,mask_array1[i,:]))
                EMP_obs = np.vstack((EMP_obs,EMP_obs1[i,:]))
                names = np.append(names,names1[i])
                FeH = np.append(FeH,FeH1[i])
                BaFe = np.append(BaFe,BaFe1[i])
                EuFe = np.append(EuFe,EuFe1[i])
                reference = np.append(reference,"Saga")
        N_EMP = len(FeH)
        print("Number of EMP stars:",N_EMP)


#Load new array with data provided by user
    mask_array_user, EMP_obs_user, EMP_err_user, N_EMP_user, names_user, FeH_user, BaFe_user, EuFe_user = EMP_user()
    print("Adding this many stars from user-defined list:",N_EMP_user)
    if(EMPset == "User-defined"):
        mask_array = mask_array_user
        EMP_obs = EMP_obs_user
        names = names_user
        FeH = FeH_user
        BaFe = BaFe_user
        EuFe = EuFe_user
        reference = np.full(N_EMP_user,"USER")
        N_EMP = N_EMP_user
    else:#add to existing set
        #First to Carbon corrections for previous set
        if True:
            EMP_obs = Ccorrection(EMP_obs, names)
        #now add new stars:
        for i in range(N_EMP_user):
            mask_array = np.vstack((mask_array,mask_array_user[i,:]))
            EMP_obs = np.vstack((EMP_obs,EMP_obs_user[i,:]))
            names = np.append(names,names_user[i])
            FeH = np.append(FeH,FeH_user[i])
            BaFe = np.append(BaFe,BaFe_user[i])
            EuFe = np.append(EuFe,EuFe_user[i])
            reference = np.append(reference,"USER")
        N_EMP += N_EMP_user


    #are these observed abundances within range of theoretical yields?
    EMPinMinMax = InRange(N_EMP,EMP_obs) #avail: 1 not avail: 0

    #write mask array for observability
    np.save("results/EMP_mask_array",[ratios,mask_array])

    if(True):
        print("write abundance data for analysis and comparison")
        file = open("results/XFe_obs.dat",'w')
#        file.write("star ")
        file.write("StarName ")
        for j in range(0,N_ratios):
            file.write(ratios[j].astype('U7')+" ")
        file.write("Reference \n")
        for i in range(N_EMP):
            file.write(names[i]+" ")
            for j in range(0,N_ratios):
                if(mask_array[i,j] <= 0):
                    file.write("-99.0 ")
                else:
                    file.write(('{:6.6}'.format(str(EMP_obs[i,j])))+" ")
            file.write(reference[i]+" \n")
        file.close()

    if(False):
#plot comparison histograms to compare observed abundances with those used for training
        train_data0 = data_train_orig[0][0]
        mask_data0 = mask_train_orig[0][0]
        SN_train_orig0 = SN_train_orig[0][0]
        print("OBS. output")
        print(np.shape(data_train_orig))
        print(np.shape(mask_train_orig))
        print(np.shape(SN_train_orig))
        print(np.shape(train_data0))
        print(np.shape(mask_data0))
        print(np.shape(SN_train_orig0))
        for j in range(0,N_ratios):
            train_plot_mono = np.ma.compressed(np.ma.masked_where(np.logical_and(SN_train_orig0>1,mask_data0[:,j] <= 0), train_data0[:,j]) )
            train_plot_multi = np.ma.compressed(np.ma.masked_where(np.logical_and(SN_train_orig0==1,mask_data0[:,j] <= 0), train_data0[:,j]) )
            saga_plot = np.ma.compressed(np.ma.masked_where(mask_array[:,j] <= 0, EMP_obs[:,j]) )

            #write out histograms for further analysis of desired PDFs
            if(True):#optional output
                EMP_hist, edges = np.histogram(saga_plot,bins=32)
                EMP_hist = EMP_hist/np.amax(EMP_hist)
                file = open("results/Histo_obs_"+EMPset+"_"+str(j)+".dat",'w')
                for i,hist_i in enumerate(EMP_hist):
                    file.write((str(0.5*(edges[i]+edges[i+1]))+" "+str(hist_i))+" \n")
                file.close()

                plt.hist(saga_plot,alpha=0.5,density=True,label="Saga, mean="+'{:5.5}'.format(str(np.mean(saga_plot))))
                plt.hist(train_plot_mono,alpha=0.5,density=True,bins=16,label="Train mono, mean="+'{:5.5}'.format(str(np.mean(train_plot_mono))))#,range=(-6,6),bins=32
                plt.hist(train_plot_multi,alpha=0.5,density=True,bins=16,label="Train multi, mean="+'{:5.5}'.format(str(np.mean(train_plot_multi))))#,range=(-6,6),bins=32

                plt.legend()
                plt.xlabel(ratios[j])
                plt.savefig("plots/Histo_obs_"+EMPset+"_"+str(j)+".pdf")
                plt.close()

    #[C/Fe] of observed stars to directly use in analysis
    CFe = EMP_obs[:,36]
    



#Specific for SVM
    N_abund = len(list_abundances)
    print("Start SVM analysis for EMP stars...")
    EMP_pred = np.zeros([N_EMP,2+N_rand])

    for i in range(N_EMP):
        for j, abundances_now in enumerate(list_abundances):
            abundances = abundances_now[0]
            #p_obs = abundances_now[1]
            X = EMP_obs[i,abundances]
            abund_max = np.max(np.abs(X))
            #type conversion only here once to save time later
            ratios_now = []
            for l in range(Ndim):
                ratios_now.append(ratios[abundances[l]].astype('U7'))

            if(abund_max<9):#abundances available
                #not available abundances have -9.99
                SVM_now = SVM_list[j][0]
                #CVacc_now = SVM_list[j][1]

                EMP_pred[i,2:] += doBootstrap([X,SVM_now,ratios_now])
                EMP_pred[i,1] += 1


    N_abund = len(list_abundances)
    #print(EMP_pred)
    np.save("results/EMP_pred_"+str(expo)+"_NSN"+str(NSNe)+"_Nabund"+str(N_abund)+".npy",EMP_pred)





    SN_EMP = np.full(N_EMP,-99, dtype=int)#necessary? -99 never used

    EMP_class = np.zeros([N_EMP,3], dtype=int)#inside data,#mono,#multi #used?

#    Nforest = np.zeros([N_EMP,Ntree*NSNe,2])#0: mono or multi, 1: weight
#    Path = np.full([N_EMP,Ntree*NSNe,MaxDepth],"00",dtype='S7')
#    FI_EMP = np.zeros([N_EMP,Ntree*NSNe,up_to_elem]) #feature importance individually for EMP stars

#Walk trees again to obtain feature importance
#This information helps to identify abundance ratios
#that have contributed most to a certain decision process
#    for i in range(NSNe):#go through ii vs. rest SNe
#        offset = i*Ntree
#        for j in range(Ntree):
#            acc = blindtest(SN_EMP,StartNodes[offset+j],EMP_obs,mask_array,offset+j,Nforest,Path,FI_EMP)

    MyPred, y_mean, y_std = MyPrediction(EMP_pred)

    

    file = open("results/EMP_monomulti_NSNe"+str(NSNe)+".dat",'w')
    file.write('Name FeH BaFe EuFe CFe NElemAvail MyPred pMulti_mean pMulti_std \n')
    N_mono  = 0.
    N_multi = 0.
    N_not = 0.


    for i in range(N_EMP):
        file.write(names[i]+" "+str(FeH[i])+" "+str(BaFe[i])+" "+str(EuFe[i])+" "+str(CFe[i])+" ")
        
        count_avail = 0
        OutOfRangeUse = 0
        for m in mask_array[i,:]:
            if(m == 1.0):#element available
                count_avail += 1
#            elif(m==-1.0 or m==-2.0):
#                count_avail += 0.5
#            else:
#                print("mask:",m)
                
        file.write(str(count_avail)+" ")

        file.write(str(MyPred[i])+" "+str(y_mean[i])+" "+str(y_std[i])+" \n")
        if(MyPred[i] == 1):
            N_mono += 1.0
        elif(MyPred[i] == 2):
            N_multi += 1.0
        elif(MyPred[i] == 0):
            N_not += 1.0

    file.close()

    print(" ")
    print("Best Guess Predictions:")
    print("Nmono:",N_mono," N_multi:",N_multi,"   f_mono:",N_mono/(N_mono+N_multi))
    print("Nnot:",N_not)

def Ccorrection(EMP_obs, EMP_names):
    #take abundances in an correct Carbon values based on Placco: http://vplacco.pythonanywhere.com/
    #surface carbon abundance is depleted. Hence, we need to add C_corr>0 to the observed value in order to obtain the initial value
    print("Doing Carbon corrections")

    C_names = np.genfromtxt('data/C-corrections.csv',usecols=(0),delimiter=',',skip_header=1,dtype="str")
    C_corr = np.genfromtxt('data/C-corrections.csv',usecols=(4),delimiter=',',skip_header=1,dtype="float")

#    NEMP = np.shape(EMP_obs)[0]
#    Ndim = np.shape(EMP_obs)[1]

    for name_corr, corr_now in zip(C_names,C_corr):
        for i,name_EMP in enumerate(EMP_names):
            if(name_corr == name_EMP):
                #print("FOUND!!!")
                for j,abund_now in enumerate(ratios):
                    if("C/" in abund_now.astype('U7')):
                        EMP_obs[i,j] += corr_now
                break

    return EMP_obs
