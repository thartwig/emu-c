#convert observed elements from form [X/Fe] to form [A/B]
#or form [X/H] to form [A/B]
import numpy as np
from parameters import *

#expects all elements
def ConvertToRatios(mask_array,obs_array,Nstars):
    obs_ratio = np.zeros([Nstars,N_ratios])
    mask_ratio = np.zeros([Nstars,N_ratios])
    for k in range (Nstars):
        ind = 0
        for i in range(N_use):
            denominator = obs_array[k,idx_use[i]-1]
            elem_i = mask_array[k,idx_use[i]-1]
            for j in range(i):
                numerator = obs_array[k,idx_use[j]-1]
                elem_j = mask_array[k,idx_use[j]-1]
                
                #e.g. [C/Na] = [C/Fe] - [Na/Fe]
                #e.g. [C/Na] = [C/H] - [Na/H]
                obs_ratio[k,ind] = numerator - denominator
                #mask
                if(elem_i > 0 and elem_j > 0):
                    mask_ratio[k,ind] = 1
                elif(elem_i > 0 and elem_j == -1):#upper limit
                    mask_ratio[k,ind] = -1
                    obs_ratio[k,ind] = -9.99
                elif(elem_i == -1 and elem_j > 0):#lower limit because denominator is lower limit
                    mask_ratio[k,ind] = -2
                    obs_ratio[k,ind] = -9.99
                else:
                    mask_ratio[k,ind] = 0
                    obs_ratio[k,ind] = -9.99

                ind += 1

    return mask_ratio, obs_ratio


#only used elements
def ConvertToRatios_user(mask_array,obs_array,Nstars):
    obs_ratio = np.zeros([Nstars,N_ratios])
    mask_ratio = np.zeros([Nstars,N_ratios])
    for k in range (Nstars):
        ind = 0
        for i in range(N_use):
            denominator = obs_array[k,i]
            elem_i = mask_array[k,i]
            for j in range(i):
                numerator = obs_array[k,j]
                elem_j = mask_array[k,j]

                #e.g. [C/Na] = [C/Fe] - [Na/Fe]
                #e.g. [C/Na] = [C/H] - [Na/H]
                obs_ratio[k,ind] = numerator - denominator
                #mask
                if(elem_i > 0 and elem_j > 0):
                    mask_ratio[k,ind] = 1
                elif(elem_i > 0 and elem_j == -1):#upper limit
                    mask_ratio[k,ind] = -1
                    obs_ratio[k,ind] = -9.99
                elif(elem_i == -1 and elem_j > 0):#lower limit because denominator is lower limit
                    mask_ratio[k,ind] = -2
                    obs_ratio[k,ind] = -9.99
                else:
                    mask_ratio[k,ind] = 0
                    obs_ratio[k,ind] = -9.99

                ind += 1

    return mask_ratio, obs_ratio
