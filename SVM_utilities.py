import numpy as np
from itertools import permutations, combinations, product
import os.path
from parameters import ratios,N_ratios, expo, NSNe
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
import time
import matplotlib
matplotlib.use('TkAgg')

def get_prob(SVM_now,X):
  if True:
    prob_now = SVM_now.predict_proba(X)[0]
    #two values with [p_class1, p_class2]

  else:
    prob = SVM_now.decision_function(X)[0]
    prob_now = np.array([0.,0.])
    if(prob < 0):
      prob_now[0] = np.abs(prob)
    else:
      prob_now[1] = prob

  return prob_now


#get list of different one-vs-one combinations that shall be tested
def get_combinations(NSNe):
    NSN_list = [x+1 for x in range(NSNe)]
    comb = combinations(NSN_list, 2)
    comb_list= []
    for now in comb:
        comb_list.append(now)
    return comb_list

#list of data of length NSNe
def load_data(WhichSet,NSNe):
    print("Loading data for",WhichSet)
    data_list = []
    for i in range(NSNe):
        file_name = "results/XFe_m_"+WhichSet+"_expo"+str(expo)+"_NSN"+str(i+1)+".dat"
        if os.path.isfile(file_name):
            data_now = np.genfromtxt(file_name,skip_header=1)
            data_list.append(data_now)
        else:
            print("WARNING!!! This file does not exist:",file_name)
    return data_list

def checkIfDuplicates(listOfElems):
    ''' Check if given list contains any duplicates '''
    if len(listOfElems) == len(set(listOfElems)):
        return False
    else:
        return True

def get_rand_abundances(Ndim=3):
    #the number of possible combinations scales approx. 78^Ndim
    #if we want to use this for Ndim>2, we need to select a subset of combinations
    #
    all_ratios = [x.astype('U7') for x in ratios]
    list_abundances = []

    idx_ratios = [i for i in range(N_ratios)]

    comb = combinations(idx_ratios, Ndim)
    for now in comb:
        list_abundances.append(now)
    print("How many combinations?",len(list_abundances))

#keep only those that are observable
    file_obs = "results/EMP_observability.dat"
    if os.path.isfile(file_obs):
        p_obs = np.genfromtxt(file_obs,usecols=1,dtype=float)
    else:
        print("ERROR!!! File not found: ",file_obs)
        print("Maybe you first need to run the code without this function to create the file?")
        print("Or run: python EMP_observability.py")
    print(p_obs)
    if (len(p_obs) != N_ratios):
        print("WARNING!!! Something is strange about the file",file_obs)

    list_rand_abundances = []
    for idx in list_abundances:
        p_now = p_obs[list(idx)]
        p_now = np.product(p_now)
        if(p_now > 10*np.random.rand()):
            list_rand_abundances.append([idx,p_now])

    print("We will explore this many combinations of abundances:",len(list_rand_abundances))

    return list_rand_abundances


def get_2D_abundances():
    list_2D_abundances = []

    idx_ratios = [i for i in range(N_ratios)]
    print("idx_ratios",idx_ratios)

    comb = combinations(idx_ratios, 2)
    for now in comb:
        list_2D_abundances.append(now)

    print(list_2D_abundances)
    print("We will explore this many combinations of abundances in 2D:",len(list_2D_abundances))

    return list_2D_abundances


def get_3D_abundances():
#we first define a subset of abundance ratios
#with the highest observabilities:
#should be in order
    best_abund = ['[Ca/Cr]','[Mg/Fe]']
#    best_abund = ['[Ca/Fe]','[Mg/Fe]','[Cr/Fe]','[Fe/Ni]','[Al/Fe]','[Fe/Co]','[Ca/Mn]']
    all_ratios = [x.astype('U7') for x in ratios]
    best_idx = []
    for abund in best_abund:
        for i,ratio in enumerate(all_ratios):
            if(abund == ratio):
                best_idx.append(i)
                break
    #print("All ratios:",all_ratios)
    print("Best ratios:",best_abund)
    list_3D_abundances = []
    for i in best_idx:
        for j in best_idx:
            if(j>=i):
                break
            for k, ratio in enumerate(all_ratios):
                #print("ratio",ratio)
                #print("2D:",abund_2D)
                if(ratio in best_abund):
                    continue#don't use it twice
                abund_3D = [i,j,k]
                #print(abund_3D)
                if(abund_3D not in list_3D_abundances):
                    print(abund_3D)
                    list_3D_abundances.append(abund_3D)

    print("We will explore this many combinations of abundances in 3D:",len(list_3D_abundances))
    return list_3D_abundances


def get_4D_abundances():
#we first define a subset of abundance ratios
#with the highest observabilities:
#should be in order
    best_abund = ['[Mg/Fe]','[Ca/Fe]','[Cr/Fe]']
#    best_abund = ['[Ca/Fe]','[Mg/Fe]','[Cr/Fe]','[Fe/Ni]','[Al/Fe]','[Fe/Co]','[Ca/Mn]']
    all_ratios = [x.astype('U7') for x in ratios]
    best_idx = []
    for abund in best_abund:
        for i,ratio in enumerate(all_ratios):
            if(abund == ratio):
                best_idx.append(i)
                break
    #print("All ratios:",all_ratios)
    print("Best ratios:",best_abund)
    list_4D_abundances = []
    for i in best_idx:
        for j in best_idx:
            if(j>=i):
                break
            for k in best_idx:
                if(k>=j):
                    break

                for l, ratio in enumerate(all_ratios):
                    print("ratio",ratio)
                    if(ratio in best_abund):
                        continue#don't use it twice
                    abund_4D = [i,j,k,l]
                    print(abund_4D)
                    if(abund_4D not in list_4D_abundances):
                        print(abund_4D)
                        list_4D_abundances.append(abund_4D)

    print("We will explore this many combinations of abundances in 4D:",len(list_4D_abundances))
    return list_4D_abundances


def get_grid(data,classes):
    #get grid for one-vs-rest with 2 possible classes:mono/multi
    
    #check how many dimension data has
    Ndim = np.shape(data)[1]
    print("How many dimensions?", Ndim)

    binary_grid = []
    d_step = 0.2#dex roughly smallest error

    grid_positions = []
    grid_lengths = []
    xmin_list = []

    for i in range(Ndim):
        x_min = np.min(data[:,i])
        xmin_list.append(x_min)
        x_max = np.max(data[:,i])
        x = np.arange(x_min-0.5*d_step,x_max+0.5*d_step,d_step)
        grid_positions.append(x)
        grid_lengths.append(len(x))

    grid_lengths.append(2)#mono/multi
    binary_grid = np.zeros(grid_lengths)
    print("shape binary_grid:",np.shape(binary_grid))
    xmin_arr = np.array(xmin_list)
    for i in range(len(data[:,0])):#loop over data
#        idx_now = [0 for x in range(Ndim+1)]
#        for j in range(Ndim):#loop over dimensions
#            idx_now[j] = int(np.round((data[i,j] - xmin_list[j])/d_step))

#        idx_now = ((data[i,:] - xmin_arr)/d_step).astype(int)
#        idx_now = (np.round((data[i,:] - xmin_arr)/d_step)).astype(int)
        idx_now = (((data[i,:] - xmin_arr)/d_step+1000.5).astype(int))-1000#faster than np.round()


        #iy = int(np.round((data[i,1] - y_min)/d_step))
        #iz = int(np.round((data[i,2] - z_min)/d_step))
        #idx_now = np.append(idx_now,int(classes[i]-1))
        #idx_now.append(classes[i]-1)
        binary_grid[tuple(idx_now)+(classes[i]-1,)] += 1


    Nmax = 1#maximum number of elements to expect
    for i in range(len(grid_lengths)-1):
        Nmax *= grid_lengths[i]
    print("grid_lengths",grid_lengths)
    print("Nmax",Nmax)
    X = np.zeros((Nmax,Ndim))
    classes = np.zeros(Nmax,dtype="int")

    idx_now = 0

    if(Ndim == 2):
        for k in range(grid_lengths[0]):
            for l in range(grid_lengths[1]):
                if(np.sum(binary_grid[k,l,:]) < 3):#at least 3 points. Otherwise scatter
                    continue
                i_max = np.argmax(binary_grid[k,l,:])
                #coordinates[i_max].append([grid_positions[0][k],grid_positions[1][l]])
                X[idx_now,0] = grid_positions[0][k]
                X[idx_now,1] = grid_positions[1][l]
                classes[idx_now] = i_max+1
                idx_now += 1
    elif(Ndim == 3):
        for k in range(grid_lengths[0]):
            for l in range(grid_lengths[1]):
                for m in range(grid_lengths[2]):
                    if(np.max(binary_grid[k,l,m,:]) == 0):
                        continue
                    i_max = np.argmax(binary_grid[k,l,m,:])
                    X[idx_now,0] = grid_positions[0][k]
                    X[idx_now,1] = grid_positions[1][l]
                    X[idx_now,2] = grid_positions[2][m]
                    classes[idx_now] = i_max+1
                    idx_now += 1
                    #coordinates[i_max].append([grid_positions[0][k],grid_positions[1][l],grid_positions[2][m]])
    elif(Ndim == 4):
        for k in range(grid_lengths[0]):
            for l in range(grid_lengths[1]):
                for m in range(grid_lengths[2]):
                    for n in range(grid_lengths[3]):
                        if(np.max(binary_grid[k,l,m,n,:]) == 0):
                            continue
                        Ntot += 1
                        i_max = np.argmax(binary_grid[k,l,m,n,:])
                        coordinates[i_max].append([grid_positions[0][k],grid_positions[1][l],grid_positions[2][m],grid_positions[3][n]])
    else:
        print("ERROR!!! This dimension is not yet supported:",Ndim)


    if (Ndim == 3):#very very slow; many many plots
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(X[classes == 1,0],X[classes == 1,1],X[classes == 1,2],marker='o')
        ax.scatter(X[classes == 2,0],X[classes == 2,1],X[classes == 2,2],marker='^')
        ax.scatter(X[classes == 3,0],X[classes == 3,1],X[classes == 3,2],marker='+')
#        plt.show()
        plt.savefig("plots/Grid_3D_"+str(Nmax)+".png")
        plt.clf()

    elif (Ndim == 2):#very very slow; many many plots
        plt.scatter(X[classes == 1,0],X[classes == 1,1],marker='o')
        plt.scatter(X[classes == 2,0],X[classes == 2,1],marker='^')
#        plt.scatter(X[classes == 3,0],X[classes == 3,1],marker='+')
#        plt.show()
        plt.savefig("plots/Grid_2D_"+str(Nmax)+".png")
        plt.clf()


    classes = classes[:idx_now]
    X = X[:idx_now,:]

    print("Histogram Classes:")
    print(np.histogram(classes,bins=2))

    print("Shapes:")
    print(np.shape(X))
    print(np.shape(classes))

    return X, classes


#once upon a time, we tried to discriminate various levels of N-fold enrichment.
#to simplify the discrimination of scatter data, we binned the data into dominating classes.
#improved accuracy, but not used now
def get_binary_grid(dataPLUS,dataMINUS):
    #print("shape:",np.shape(dataPLUS))#(1433, 3)
    binary_grid = []
    d_step = 0.2#dex roughly smallest error
    x_min = np.min([dataPLUS[:,0],dataMINUS[:,0]])
    x_max = np.max([dataPLUS[:,0],dataMINUS[:,0]])
    y_min = np.min([dataPLUS[:,1],dataMINUS[:,1]])
    y_max = np.max([dataPLUS[:,1],dataMINUS[:,1]])
    z_min = np.min([dataPLUS[:,2],dataMINUS[:,2]])
    z_max = np.max([dataPLUS[:,2],dataMINUS[:,2]])
    x = np.arange(x_min-0.5*d_step,x_max+0.5*d_step,d_step)
    y = np.arange(y_min-0.5*d_step,y_max+0.5*d_step,d_step)
    z = np.arange(z_min-0.5*d_step,z_max+0.5*d_step,d_step)

    Nx, Ny, Nz = len(x), len(y), len(z)
    binary_grid = np.zeros([Nx,Ny,Nz,2])
    for i in range(len(dataPLUS[:,0])):
        ix = int(np.round((dataPLUS[i,0] - x_min)/d_step))
        iy = int(np.round((dataPLUS[i,1] - y_min)/d_step))
        iz = int(np.round((dataPLUS[i,2] - z_min)/d_step))
        binary_grid[ix,iy,iz,0] += 1
    for i in range(len(dataMINUS[:,0])):
        ix = int(np.round((dataMINUS[i,0] - x_min)/d_step))
        iy = int(np.round((dataMINUS[i,1] - y_min)/d_step))
        iz = int(np.round((dataMINUS[i,2] - z_min)/d_step))
        binary_grid[ix,iy,iz,1] += 1

    X1,Y1,Z1,X2,Y2,Z2 = [],[],[],[],[],[]
    for k in range(Nx):
        for l in range(Ny):
            for m in range(Nz):
                if(binary_grid[k,l,m,0] > binary_grid[k,l,m,1]):
                    X1.append(x[k])
                    Y1.append(y[l])
                    Z1.append(z[m])
                elif(binary_grid[k,l,m,1] > binary_grid[k,l,m,0]):
                    X2.append(x[k])
                    Y2.append(y[l])
                    Z2.append(z[m])

    N1 = len(X1)
    N2 = len(X2)
    print(N1/N2)
    if(min([X1,X2])==0):
        print("WARNING!!! At least one of the binary subsets is empty",X1,X2)

    classPLUS  = np.full(N1,1)
    classMINUS = np.full(N2,-1)

    if False:#very very slow; many many plots
        fig = plt.figure()
        ax = fig.add_subplot(111, projection='3d')
        ax.scatter(X1,Y1,Z1,marker='o')
        ax.scatter(X2,Y2,Z2,marker='^')
        plt.show()
    #plt.savefig("test.png")
        plt.clf()

    X = np.append(X1,X2)
    Y = np.append(Y1,Y2)
    Z = np.append(Z1,Z2)
    classes = np.append(classPLUS,classMINUS)
    X = np.stack((X, Y, Z), axis=-1)

    return X, classes

def p2_to_p1(p2):
    #p2 is 2D array that containy p_class1 and p_class2
    #convert it into one number between -1 and 1.
    return (2*p2[:,0]-1)


def write_to_file(NSN_combination,abundances_3D,data_train0,CV_classes,y_pred,acc):
    file_name = "results/XYZ/XYZ_3D_CV_acc"+str(acc)+"_"
    file_name += str(NSN_combination[0])+str(NSN_combination[1])
    file_name += "_abund_"+str(abundances_3D[0])+str(abundances_3D[1])+str(abundances_3D[2])+".dat"
    file = open(file_name,'w')
    for abu in ratios[abundances_3D].astype('U7'):
        file.write(abu+" ")
    file.write("GroundTruth Predicted \n")
    for i in range(len(y_pred)):
        file.write(str(data_train0[i,0])+" "+
                str(data_train0[i,1])+" "+
                str(data_train0[i,2])+" "+
                str(CV_classes[i])+" "+
                str(y_pred[i])+" \n")
    file.close()


def plot_DecisionBoundary_2D(clf,dx=3.0):
    h=0.1
    xx, yy = np.meshgrid(np.arange(-1.0*dx, dx+h, h), np.arange(-1.0*dx, dx+h, h))

    Z = clf.predict(np.c_[xx.ravel(), yy.ravel()])
    Z = Z.reshape(xx.shape)
    #cmap=plt.cm.twilight
    return plt.contourf(xx, yy, Z,cmap=plt.cm.coolwarm,alpha=0.5)

def Confusion(GroundTruth,Pred):
    '''
                 mono_GT    multi_GD
    N/A           (0,0)       (0,1)
    mono_pred     (1,0)       (1,1)
    multi_pred    (2,0)       (2,1)
    '''
    Npred = len(Pred)
    if(Npred != len(GroundTruth)):
        print("Error: different lengths")
    MyConf = np.zeros([2+1,2])#+1 for not classified
    for i in range(Npred):
        k = int(GroundTruth[i])-1
        j = int(Pred[i])
        MyConf[j,k] += 1
    print(MyConf)
    f_classified = MyConf[1,0]+MyConf[2,1]+MyConf[1,1]+MyConf[2,0]
    f_corr = (MyConf[1,0]+MyConf[2,1])/f_classified
    asy_diff = np.abs(MyConf[1,1]-MyConf[2,0])/f_classified
    asy_corr = np.abs(MyConf[1,0]-MyConf[2,1])/f_classified
    print("Correct:",f_corr)
    print("Asymmetry wrong:",asy_diff)
    print("Asymmetry correct:",asy_corr)
    print("Not classified:",1-f_classified/np.sum(MyConf))
    print("f_multi=",(MyConf[2,0]+MyConf[2,1])/(MyConf[1,0]+MyConf[2,1]+MyConf[1,1]+MyConf[2,0]))

def MyPrediction(X_pred,sigma = 0.0):
    '''
    convert bootstrap array into predictions
    X_pred : (Ntot,2+N_rand)
    sigma: confidence threshold to accept classification
    '''
    Ntot = np.shape(X_pred)[0]
    N_rand = np.shape(X_pred)[1]-2

    y_pred = np.zeros(Ntot)
    y_mean = np.zeros(Ntot)
    y_std = np.zeros(Ntot)

    X = X_pred[:,2:]
    #y_true = X_pred[:,0]
    Npred = X_pred[:,1]
    for i in range(Ntot):
        if(Npred[i] > 0):
            X[i,:] = (X[i,:]/float(Npred[i]))-1.0 #normalise to 0 to 1

    for i in range(Ntot):
        x_mean = np.mean(X[i,:])
        x_std = np.std(X[i,:])
        y_mean[i] = x_mean
        y_std[i] = x_std
        if(np.abs(x_mean-0.5) > sigma*x_std):
            if(x_mean<0.5):#mono
                y_pred[i] = 1
            else:
                y_pred[i] = 2
        else:
            y_pred[i] = 0

    #bootstrap: first average over samples
    fmono_arr = []
    for i in range(N_rand):
        Nmono  = 0.
        Nmulti = 0.
        for j in range(Ntot):
            if(Npred[j] > 0):
                if(X[j,i] > 0.5):
                    Nmulti += 1.0
                elif(X[j,i] < 0.5):
                    Nmono += 1.0
        fmono_now = 100*Nmono/(Nmono+Nmulti)
        fmono_arr.append(fmono_now)

    print("Please note that the following results might contain stars with <=3 observed elements.")
    print("The published results and f_mono ratio are only based on stars with >3 observed elements.")
    print("Moreover, the RNG of the bootstrap sampling can cause small differences to the published results.")
    print("   ")
    print("Results from Bootstrap sampling are an average over many realisations:")
    print("BOOTSTRAP: fmono =",np.mean(fmono_arr),"% +/-",np.std(fmono_arr),"%")

    return y_pred, y_mean, y_std
