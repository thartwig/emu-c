import numpy as np
#import random
import glob
from parameters import L_NSN,SNset,NSNe,N_ratios,N_use,idx_use
from parameters import MinMaxAvail,ratios,IsotopeMass,asplund_all
from parameters import ftrain, fCV1, fCV2
from GetUncertainty import get_scatter_th, get_scatter_obs

def constructdata(expo,NSN_now):
    print("NSN_now: ",NSN_now)

    #we will test only mono vs. multi
    #mono shall have same number of examples as multi
    #hence, mono must be larger by some factor
    expo_now = expo
    if(NSN_now == 1):#mono
        if(NSNe == 3):
            expo_now += 1#factor two more examples
        elif(NSNe == 5):
            expo_now += 2#factor four more examples
        else:
            print("WARNING!!! This NSNe is not yet implemented:",NSNe)
        expo_now = expo_now#-2#change it for different f_mono_train

    print("constructing train and blind test data...")
    #yields by Heger & Woosley, The Astrophysical Journal, Volume 724, Issue 1, pp. 341-373 (2010).
    if (SNset == "HW10"):
        m_mono_HW10 = np.loadtxt('data/HW10_S4.dat',skiprows=1)#.T
        m_mono = m_mono_HW10

    #yields by Ishigaki et al., The Astrophysical Journal, Volume 857, Issue 1, article id. 46, 21 pp. (2018).
    #these yields are not public. They may be shared upon reasonable request to
    #Miho Ishigaki <miho.ishigaki@nao.ac.jp>
    elif (SNset == "Miho18"):
        m_mono_Miho18 = np.loadtxt('data/Ishigaki18.dat',skiprows=1)#.T
        m_mono = m_mono_Miho18
    else:
        "ERROR: SN yields not yet implemented! Try HW10 or Miho18."

    print("Write Masses to file to check for faint SNe")
    m_sum = np.sum(m_mono[:,idx_use],axis=1)
    np.save("results/m_sum_chi2_mean15",m_sum)

    print("independent SN models available: ",len(m_mono[:,25]))
#augment mock data
    for i in range(expo_now):
        m_mono = np.append(m_mono,m_mono, axis=0)
        
    Nlines = len(m_mono[:,25])
    N_train = int(Nlines*ftrain)
    N_CV1 = int(Nlines*fCV1)
    N_CV2 = int(Nlines*fCV2)
    N_test = Nlines - N_train - N_CV1 - N_CV2

    print("Number of SNe total: ",Nlines)
    print("Number of SNe for training: ",N_train)
    print("Number of SNe for cross-validation: ",N_CV1,N_CV2)
    print("Number of SNe for blind test: ",N_test)

    N_avail = len(m_mono[0,:])
    m_mm = np.zeros([Nlines,N_avail])
    
    #create multi enriched random pairs
    np.random.shuffle(m_mono)
    m_mono_shuffle = np.copy(m_mono)
    np.random.shuffle(m_mono_shuffle)
    m_multi = np.copy(m_mono)#multi still mono
   

    for i in range(NSN_now-1):
        #enrich by one more SN
        m_mono_shuffle = np.copy(m_mono)
        np.random.shuffle(m_mono_shuffle)
        if(NSN_now == 2):
            print("Write out mass ratios of 2-fold enriched to check for faint SNe")
            print("Nlines:",Nlines)
            f_faint = np.zeros(Nlines)
            for j in range(Nlines):
                mSum1 = np.sum(m_multi[j,idx_use])
                mSum2 = np.sum(m_mono_shuffle[j,idx_use])
                f_faint[j] = np.min([mSum1,mSum2])/(mSum1+mSum2)
            np.save("results/f_faint",f_faint)

        m_multi = m_multi+m_mono_shuffle
        print("m_multi now contains combination of how many SNe?",i+2)
    m_mm = m_multi
    
    #array of different element ratios
    m_ratio = np.zeros([Nlines,N_ratios])

    m_min = 1e-32 #lower floor
#    m_Fe=np.copy(m_mono[:,25])
#    print("Fe:",m_Fe)
#    Nlines = len(m_Fe)
    for k in range (Nlines):
        ind = 0
        for i in range(N_use):
            m_1 = max([m_mm[k,idx_use[i]-1],m_min])
            for j in range(i):
                m_2 = max([m_mm[k,idx_use[j]-1],m_min])

                m_ratio[k,ind] = np.log10(m_2/m_1)-np.log10(IsotopeMass[idx_use[j]-1]/IsotopeMass[idx_use[i]-1]) + asplund_all[idx_use[j]-1] - asplund_all[idx_use[i]-1]
                ind += 1


    print("Element-specific errors for all yields")
    for i in range (Nlines):
        for j in range(0,N_ratios):
#            if(i < N_train):
            m_ratio[i,j] = m_ratio[i,j] + get_scatter_th(ratios[j].astype('U7'))# + get_scatter_obs(ratios[j].astype('U7'))
#            else:
#                m_ratio[i,j] = m_ratio[i,j] + get_scatter_th(ratios[j].astype('U7')) + get_scatter_obs(ratios[j].astype('U7'))


    #correct bias in mock data
#    MeanShift = [-0.159, -0.137, -0.004, -0.045, 0.252, 0.089, 0.069, 0.418, 0.387, 0.297, -0.231, 0.339, 0.15, 0.048, -0.28, -0.177, 0.243, -0.022, -0.143, -0.448, -0.178, 0.08, 0.469, 0.429, 0.25, -0.016, 0.245, 0.421, -0.264, 0.253, 0.119, -0.015, -0.3, -0.018, 0.126, -0.308, 0.177, 0.578, 0.253, 0.166, -0.115, 0.133, 0.311, -0.086, 0.197, -0.406, 0.245, -0.055, -0.152, -0.455, -0.153, 0.03, -0.442, -0.141, -0.338, 0.048, 0.609, 0.395, 0.254, -0.017, 0.278, 0.418, 0.01, 0.29, 0.1, 0.448, -0.494, 0.127, 0.03, -0.028, -0.327, 0.073, 0.092, -0.245, 0.043, -0.25, -0.024, -0.358] 
    StdRatio = [1.67, 1.036, 1.041, 1.688, 1.432, 0.547, 0.916, 0.87, 0.648, 0.418, 1.566, 1.462, 0.593, 0.724, 0.596, 1.658, 1.464, 0.624, 0.632, 0.452, 0.966, 1.322, 1.197, 0.644, 0.616, 0.471, 0.912, 0.598, 1.279, 1.322, 0.687, 0.695, 0.543, 0.96, 0.831, 0.614, 1.828, 1.517, 0.71, 0.618, 0.498, 0.964, 0.671, 0.719, 0.968, 1.017, 1.05, 0.698, 0.685, 0.581, 0.933, 0.667, 0.759, 0.853, 0.876, 1.361, 1.091, 0.741, 0.591, 0.474, 0.799, 0.639, 0.562, 0.611, 0.71, 0.64, 0.996, 0.929, 0.59, 0.576, 0.576, 0.95, 0.569, 0.767, 0.821, 0.838, 0.777, 0.559]
#    for j in range(0,N_ratios):
#        print("Now correcting ",ratios[j])
#        m_ratio[:,j] += MeanShift[j]
    if False:
        m_ratio[:,37] += 0.578
        m_ratio[:,41] += 0.133
        m_ratio[:,36] += 0.177
        m_ratio[:,40] -=0.115
        m_ratio[:,39] +=0.166
        m_ratio[:,48] -= 0.152
        m_ratio[:,5] +=0.089
        m_ratio[:,10] -=0.231
        m_ratio[:,17] -=0.022
        m_ratio[:,38] +=0.253
   
        for i in [37,41,36]:
            m_ratio[:,i] = RescaleSigma(m_ratio[:,i],StdRatio[i])

    #check min/max available abundance
    for i in range(0,N_ratios):
        MinMaxAvail[i,0] = min(m_ratio[:,i])
        MinMaxAvail[i,1] = max(m_ratio[:,i])
#        print(ratios[i],MinMaxAvail[i,0],MinMaxAvail[i,1])

    if(NSN_now == 2):
        print("Write out mass ratios of 2-fold enriched to check for faint SNe")
        write_XFe_file("Faint",NSN_now,ratios,m_ratio,0,Nlines,expo)
    write_XFe_file("Train",NSN_now,ratios,m_ratio,0,N_train,expo)
    write_XFe_file("CV1",NSN_now,ratios,m_ratio,N_train,N_train+N_CV1,expo)
    write_XFe_file("CV2",NSN_now,ratios,m_ratio,N_train+N_CV1,N_train+N_CV1+N_CV2,expo)
    write_XFe_file("Test",NSN_now,ratios,m_ratio,N_train+N_CV1+N_CV2,Nlines,expo)


    print("constructed train and blind test data")
    return Nlines,N_train,N_CV1,N_CV2,N_test

def RescaleSigma(x,StdRatio):#change width of distributions to match observed data
    xmean = np.mean(x)
    x = x - xmean
    x = x * StdRatio
    x = x + xmean
    return x

def write_XFe_file(SetName,NSN_now,ratios,m_ratio,Nfrom,Nto,expo):
        file = open("results/XFe_m_"+SetName+"_expo"+str(expo)+"_NSN"+str(NSN_now)+".dat",'w')
        file.write("#")
        for j in range(0,N_ratios):
                file.write(ratios[j].astype('U7')+" ")
        file.write("\n")
        for i in range(Nfrom,Nto):
            for j in range(0,N_ratios):
                #file.write(('{:6.6}'.format(str(m_ratio[i,j])))+"  ")
                file.write(('{:.3E}'.format(m_ratio[i,j]))+" ")
            file.write("\n")
        file.close()


def getmask(lines,N_ratios):
    mask = np.zeros((lines,N_ratios),dtype=int)
#    with open('data/mask_saga.dat') as file:
    with open('data/mask_miho.dat') as file:
        dummy = file.readline()#first line
        array = file.readlines()
    Nobs = len(array)
    
    #construct mask for different elemental ratios
    mask_obs = np.zeros([Nobs,N_ratios])
    observability = np.zeros(N_ratios)
    for k in range(Nobs):
        ind = 0
        tempA = array[k].split()
#        print(int(tempA[21-1]))#Sc
#        print(int(tempA[22-1]))#Ti
#Scandium&Titanium are underproduced in theoretical yields (Tominiaga+07)
#therefore, we only treat them as lower limits:
        tempA[20] = -2
        tempA[21] = -2
        #[j/i]
        for i in range(N_use):
            elem_i = int(tempA[idx_use[i]-1])
            for j in range(i):
                elem_j = int(tempA[idx_use[j]-1])
                if(elem_i > 0 and elem_j > 0):
                    mask_obs[k,ind] = 1
                    observability[ind] += 1.0
#upper limits from observations
                elif(elem_i > 0 and elem_j == -1):#upper limit
                    mask_obs[k,ind] = -1
                    observability[ind] += 0.5
                elif(elem_i == -1 and elem_j > 0):#lower limit because denominator is lower limit
                    mask_obs[k,ind] = -2
                    observability[ind] += 0.5
#lower limits from theoretical uncertainty
                elif(elem_i > 0 and elem_j == -2):#upper limit
                    mask_obs[k,ind] = -2
                    observability[ind] += 0.5
                elif(elem_i == -2 and elem_j > 0):#lower limit because denominator is lower limit
                    mask_obs[k,ind] = -1
                    observability[ind] += 0.5
                else:
                    mask_obs[k,ind] = 0
                ind += 1
    
    observability /= float(Nobs)
    
    for i in range(lines):
        j = int(np.floor(Nobs*np.random.rand()))
        #0: not available
        #>0: available, number of lines
        #-1: upper limit
        mask[i,:] = mask_obs[j,:]
    return mask, observability


def scatter_th(jj):
    sigma = sigma_th
    return np.random.normal(loc=0.0, scale=sigma, size=None)

def scatter_obs(jj):
    sigma = sigma_obs
    return np.random.normal(loc=0.0, scale=sigma, size=None)
