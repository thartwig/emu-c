import numpy as np
import matplotlib
import os.path
import multiprocessing
#matplotlib.use('pdf')#to enable X11forward
matplotlib.use('TkAgg')

from constructtrainingtest import constructdata
from convert_Ishigaki import constructmiho, mask_miho
from parameters import expo, NSNe, ratios, N_ratios, Ndim, N_rand
from AbundInRange import InRange
from EvaluateEMP_SVM import EvalEMP_SVM
from SVM_utilities import get_combinations, load_data, plot_DecisionBoundary_2D, MyPrediction
from SVM_utilities import p2_to_p1, write_to_file, get_rand_abundances, Confusion, get_prob
from SVM import doSVMmagic,SVM_parallel
from GetUncertainty import doBootstrap
from sklearn import metrics
from matplotlib import pyplot as plt
from matplotlib.pyplot import figure
np.random.seed(4)


#check if output directories exist
dir_list = ["results","plots"]
for dir_now in dir_list:
  if(not os.path.isdir(dir_now)):
    print("### WARNING ###")
    print("Make sure that this directory exists in the right location: "+dir_now)




#The following two functions need to be executed only once.
#The created files can then be reused for further runs.
#construct table from Miho Ishigaki's data files
### CAUTION: data for this function not public. See README file. ###
#constructmiho()

#construct mask from Miho Ishigaki's compilation of observed EMP stars
#mask_miho()


FI_SVM = np.zeros((N_ratios,2))

#Our SVM will operate in 3D
#we have 78 different abundance ratios and could construct O(78^3)
#different 3D abundance combinations.
#1) This is too much and will take too long
#2) Many abundance ratios have a low observability and
#   therefore may not provide a lot of additional insight
#list_abundances = get_rand_abundances(Ndim=Ndim)
#N_abund = len(list_abundances)
#print("Number abundances:",N_abund)

SVM_file = "results/SVM_lists_expo"+str(expo)+"_NSN"+str(NSNe)+"_Ndim"+str(Ndim)+".npy"

if (os.path.isfile(SVM_file)):
    print("SVM list already exists")
    print("Loading SVM list...")
    SVM_list = np.load(SVM_file,allow_pickle=True)
    list_abundances = []
    N_abund = len(SVM_list)
    for SVM_now in SVM_list:
        list_abundances.append(SVM_now[2])
else:
    #construct training and test data
    if (os.path.isfile("results/XFe_m_Train_expo"+str(expo)+"_NSN"+str(NSNe)+".dat")):
        print("Files exist already")
    else:
        for NSN_now in range (1,NSNe+1):
            print("NSN_now in loop: ",NSN_now)
            Nlines,N_train,N_CV1,N_CV2,N_test = constructdata(expo,NSN_now)
        print(Nlines,N_train,N_CV1,N_CV2,N_test)

    #list_combinations = get_combinations(NSNe)
    #print("list_combinations",list_combinations)

    #list of training data of length NSNe
    data_train = load_data("Train",NSNe)
    data_CV1   = load_data("CV1",NSNe)
    #list with lists of style [(idx_tuple),observability]
    #list_abundances = get_rand_abundances(Ndim=Ndim)
    #list_abundances = get_3D_abundances()
    if(Ndim == 2):
        list_abundances = np.load("data/abundace_list_EMP_2D_v1.npy",allow_pickle=True)
        list_abundances[0] = [(42,3), 0.5]
        list_abundances[1] = [(42,4), 0.5]
        print(list_abundances)
    elif(Ndim == 4):
        list_abundances = np.load("data/abundance_list_EMP_4D_10.npy",allow_pickle=True)#FIDUCIAL with 10combinations!
    elif(Ndim == 26):
        list_abundances = np.load("data/abundance_list_EMP_26D_3.npy",allow_pickle=True)
    elif(Ndim == 32):
#        list_abundances = np.load("data/abundance_list_EMP_32D_64.npy",allow_pickle=True)
        list_abundances = np.load("data/abundance_list_EMP_32D_18.npy",allow_pickle=True)
    elif(Ndim == 78):
        list_abundances = np.load("data/abundance_list_EMP_78D_1.npy",allow_pickle=True)
    N_abund = len(list_abundances)
    print("Number abundances:",N_abund)

    print("Start Training")
#TRAINING
#save SVM objects to use later
#list of lists of trained SVM
    SVM_list = []
    acc_train_list = []#on original data
    acc_CV_list = []#cross-validation

    if True:
        data = data_train[0]
        N_train0 = len(data[:,0])
        classes_train = np.full(N_train0,1)#mono
        N_train1 = 0
        for i in range(1,NSNe):
            print(i)
            data_now = data_train[i]
            N_train = len(data_now[:,0])
            N_train1 += N_train
            data = np.vstack((data,data_now))
            classes_train = np.append(classes_train,np.full(N_train,2))#2 for multi
        f_mono_train = N_train0/(N_train0+N_train1)
        print("Mono Fraction Training:",f_mono_train)


        CV1 = data_CV1[0]
        N_CV = len(CV1[:,0])
        CV_classes = np.full(N_CV,1)
        for i in range(1,NSNe):
            print(i)
            CV_now = data_CV1[i]
            N_CV = len(CV_now[:,0])
            CV1 = np.vstack((CV1,CV_now))#mono
            CV_classes = np.append(CV_classes,np.full(N_CV,2))#multi



        #put all arguments into one list
        MyList = [[x,data,classes_train] for x in list_abundances ]

        n_jobs = len(list_abundances)
        print("Start parallel with #CPUs:",n_jobs)
        pool = multiprocessing.Pool(n_jobs)
        clf_parallel_temp = pool.map(SVM_parallel,MyList)
        pool.close()

        #check if parallel has messed up order
        clf_parallel = []
        for i in range(len(list_abundances)):
            if(list_abundances[i][0] != clf_parallel_temp[i][0]):
                print("ERROR: abundance order not the same:")
                print(list_abundances[i])
                print(clf_parallel_temp[i][0])
            clf_parallel.append(clf_parallel_temp[i][1])

        i_plot=0
        for i, abundances_now in enumerate(list_abundances):
            i_plot += 1
            abundances = abundances_now[0]
            print(abundances)
            for abuNow in abundances:
                print(ratios[abuNow])
            data_now = data[:,abundances]
            clf = clf_parallel[i] #doSVMmagic(data_now,classes_train)
            #plot_DecisionBoundary_2D(clf)
            if(Ndim == 2):
                figure(num=None, figsize=(5, 3), dpi=200, facecolor='w', edgecolor='k')
                plt.scatter(data_now[classes_train == 1,0],data_now[classes_train == 1,1],marker='o',facecolors='none', edgecolors="orange",label="mono-enriched",s=8)
                plt.scatter(data_now[classes_train == 2,0],data_now[classes_train == 2,1],marker='^',facecolors='none', edgecolors="blue",label="multi-enriched",s=8)
                plt.xlabel(ratios[abundances[0]].astype('U7'))
                plt.ylabel(ratios[abundances[1]].astype('U7'))
                #plt.xlim([-3,3])
                #plt.ylim([-3,3])
                plt.tight_layout()
                plt.legend()
                plt.savefig("plots/DecisionXY_"+str(i_plot)+".png")

                #plot_DecisionBoundary_2D(clf)
                #plt.savefig("plots/DecisionBoundary_"+str(i_plot)+".png")
                plt.clf()


            print("Training Acc...")
            y_pred = clf.predict(data_now)
            acc_train = metrics.accuracy_score(classes_train, y_pred)
            acc_train_list.append(acc_train)
            #print("train0:",metrics.confusion_matrix(classes_train, y_pred))


            print("CV1 Acc...")
            y_pred = clf.predict(CV1[:,abundances])
    # Cross-Valication Accuracy: how often is the classifier correct?
            acc_CV = metrics.accuracy_score(CV_classes, y_pred)
            acc_CV_list.append(acc_CV)
            #print("CV:",metrics.confusion_matrix(CV_classes, y_pred))
            print(acc_train,acc_CV)



            SVM_list.append([clf,acc_CV,abundances_now])#acc_CV to weight prediction accordingly

            FI_SVM[abundances,0] += acc_CV
            FI_SVM[abundances,1] += 1


    #save list
    np.save(SVM_file,SVM_list)
    #print(FI_SVM)

    file = open("results/FI_SVM.dat",'w')
    file.write("Ratio SumAcc N \n")
    for i in range(N_ratios):
        file.write(ratios[i].astype('U7')+" "+str(FI_SVM[i,0])+" "+str(FI_SVM[i,1])+" \n")
    file.close()

    print("Train, CV")
    for listNow in [acc_train_list,acc_CV_list]:
        print("Mean Accuracy:",np.mean(listNow))
        print("Median Accuracy:",np.median(listNow))

        #plt.hist(acc_CV_list,label="Acc. cross-validation",alpha=0.5)
        #plt.legend()
        #plt.tight_layout()
        #plt.savefig("plots/Acc_SVM_hist.pdf")



#Do second cross-validation only if necessary
CV2_pred_file = "results/CV2_pred_"+str(expo)+"_NSN"+str(NSNe)+"_Nabund"+str(N_abund)+".npy"

#To the accuracy of bagging method

if (os.path.isfile(CV2_pred_file)):
    print("CV2 files already exists")
else:
    data_CV2   = load_data("CV2",NSNe)
    CV2 = data_CV2[0]
    N_CV2 = len(CV2[:,0])
    CV2_classes = np.full(N_CV2,1)
    for i in range(1,NSNe):
        print(i)
        CV_now = data_CV2[i]
        N_CV2 = len(CV_now[:,0])
        CV2 = np.vstack((CV2,CV_now))
        CV2_classes = np.append(CV2_classes,np.full(N_CV2,2))#2 because all multi
    N_CV2 = len(CV2_classes)
    print("Number of examples in CV2:",N_CV2)
    f_multi = np.sum(CV2_classes-1.0)/float(N_CV2)
    print("f_multi:",f_multi)
    f_mono = 1.0 - f_multi
    print("f_mono:",f_mono)


    #study two-fold enriched faint SNe separately
    data_faint = load_data("Faint",NSNe)
    faint = data_faint[0]
    #print("FAINT data:",faint)
    Nlines = np.shape(faint)[0]#not defined if saved data is loaded
    faint_prob = np.zeros([Nlines,2])



    #CV2_prob = np.zeros([N_CV2,2+1])
    #CV2_prob[:,0] = CV2_classes

    #CV2_weighted = np.zeros([N_CV2,2+1])
    #CV2_weighted[:,0] = CV2_classes

    CV2_arr = np.zeros([N_CV2,4*N_abund+1])
    CV2_arr[:,0] = CV2_classes


    CV2_pred = np.zeros([N_CV2,N_rand+2])
    CV2_pred[:,0] = CV2_classes


    if False:
        #which index/element shall be shuffled for Permutation Importance Test?
        #idx_shuffle = [0,1,3,6,10,15,21,28,36,45,55,66]#C
        #idx_shuffle = [0,2,4,7,11,16,22,29,37,46,56,67]#O
        #idx_shuffle = [1,2,5,8,12,17,23,30,38,47,57,68]#Na
        #idx_shuffle = [3,4,5,9,13,18,24,31,39,48,58,69]#Mg
        #idx_shuffle = [6,7,8,9,14,19,25,32,40,49,59,70]#Al
        #idx_shuffle = [10,11,12,13,14,20,26,33,41,50,60,71]#Si
        #idx_shuffle = [15,16,17,18,19,20,27,34,42,51,61,72]#Ca
        #idx_shuffle = [21,22,23,24,25,26,27,35,43,52,62,73]#Cr
        #idx_shuffle = [36,37,38,39,40,41,42,43,44,54,64,75]#Fe
        #idx_shuffle = [45,46,47,48,49,50,51,52,53,54,65,76]#Co
        #idx_shuffle = [66,67,68,69,70,71,72,73,74,75,76,77]#Zn

        for idx_s in idx_shuffle:
            x_temp = CV2[:,idx_s]
            np.random.shuffle(x_temp)
            CV2[:,idx_s] = x_temp

    for j, abundances_now in enumerate(list_abundances):
        #go through abundances
        print(j)
        abundances = abundances_now[0]
        p_obs = abundances_now[1]
        SVM_now = SVM_list[j][0]
        CVacc_now = SVM_list[j][1]

        #type conversion only here once to save time later
        ratios_now = []
        for l in range(Ndim):
            ratios_now.append(ratios[abundances[l]].astype('U7'))

        MyList = []
        MyIdx = []
        pool = multiprocessing.Pool(64)
        for i,X in enumerate(CV2[:,abundances]):
            if(p_obs > np.random.rand()):# assume all are observable? change here to True
                MyList.append([X,SVM_now,ratios_now])#put all arguments into one list
                MyIdx.append(i)
#                CV2_pred[i,2:] += doBootstrap(X,SVM_now,ratios_now)
                CV2_pred[i,1] += 1

        MyResult = pool.map(doBootstrap,MyList)
        pool.close()
        for i,j in enumerate(MyIdx):#put into correct position of array
            CV2_pred[j,2:] += MyResult[i]


        if False:
          for i,X in enumerate(faint[:,abundances]):
            if(p_obs > np.random.rand()):
#Reshape your data either using array.reshape(-1, 1) if your data has a single feature or array.reshape(1, -1) if it contains a single sample.
              X = X.reshape(1, -1)
              y_pred = SVM_now.predict(X)
              prob_now = get_prob(SVM_now,X)
              faint_prob[i,:] += prob_now

    #print("CV2 mean:",np.mean(CV2_arr))
    #print("CV2 min:",np.min(CV2_arr))
    #print("CV2 max:",np.max(CV2_arr))
    #print(CV2_prob)
    #print(CV2_weighted)
    #print(CV2_pred)
    #np.save("results/CV2_arr_"+str(expo)+"_NSN"+str(NSNe)+"_Nabund"+str(N_abund)+".npy",CV2_arr)
#    np.save(CV2_prob_file,CV2_prob)
    #np.save("results/CV2_weighted_"+str(expo)+"_NSN"+str(NSNe)+"_Nabund"+str(N_abund)+".npy",CV2_weighted)
    np.save("results/CV2_pred_"+str(expo)+"_NSN"+str(NSNe)+"_Nabund"+str(N_abund)+".npy",CV2_pred)
#    np.save("results/faint_prob_"+str(expo)+"_NSN"+str(NSNe)+"_Nabund"+str(N_abund)+".npy",faint_prob)

    MyPred, y_mean, y_std = MyPrediction(CV2_pred)


    Confusion(CV2_classes,MyPred)

f_mono = 0.5
EvalEMP_SVM(list_abundances,SVM_list,f_mono)

print("### CODE FINISHED SUCCESSFULLY ###")
