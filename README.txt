### Emp MUltiplicity Classifier ###
This code was used to produce the results presentes in the publication Hartwig+23, ApJ accepted
It is a supervised machine learning model that is trained on synthetic yields of Population III supernovae (SNe).
The model then predicts if an extremely metal-poor (EMP) star has formed out of gas that was enriched by
one SN (mono-enriched) or by multiple SNe (multi-enriched).
This code can be used to classify newly observed EMP stars (see "Using your own EMP abundances" below).


### Pop III SN yields ###
In our study we use the yields by
Ishigaki et al., The Astrophysical Journal, Volume 857, Issue 1, article id. 46, 21 pp. (2018).
These yields are not public.
They may be shared upon reasonable request to Miho Ishigaki <miho.ishigaki@nao.ac.jp>

If you run the fiducial model of EMu-C, it loads our pre-trained model from the file results/SVM_list_expo6_NSN5_Ndim4.npy .
This model is based on the non-public yields by Ishigaki+18.

We have also implemented the public yields by
Heger & Woosley, ApJ, Volume 724, Issue 1, pp. 341-373 (2010).
The file HW10_S4.dat contains Pop III SN yields based on their S4 model.
However, please not that this model contains parameter combinations that result in unrealistic abundance ratios.
The user has to make sure to select a resonable subset of these yields, as we do in our original paper.


### Required Python Libraries ###
Not-so-standard ones: scipy, sklearn
Can be installed via:
pip install scikit-learn
pip install scipy


### Run the Code ###
The code was tested with python version 2.7, 3.6, 3.7
set desired parameters in parameters.py
run code with
python main.py 


### Folder Structure ###
You should create the folder 'plots' if you want to train and rerun the model from scratch.
The folder structure should look like this:
ThisFolder
-- *.py (all python files)
-- data (folder to store input data)
-- results (folder to store results)
-- plots (folder to store plots)




### Outputs ###
plots/
[Only produced if ran from scratch]
Histo_obs_*.pdf: comparison of the distirbution of available abundance ratios between mock observations and observed EMP stars
Observability.pdf: Observability of the top 16 abundance ratios


results/
EMP_monomulti_NSNe*.dat: Main results, classification of EMP stars
XFe_obs.dat: Used abundance ratios of classified EMP stars
SVM_lists_expo6_NSN5_Ndim4.npy: SVM-objects of pre-trained models
CV2_pred_6_NSN5_Nabund10.npy: results of 2nd cross-validation
EMP_mask_array.npy: mask arrays that contain information about the observaibility of abundance ratios
EMP_pred_6_NSN5_Nabund10.npy: detailed predictions of EMP stars
[The following are only produced if ran from scratch]
Observability.dat: observability of abundance ratios in per cent of EMP stars
XY_{N}.dat: abundances used in N-fold enriched vs. rest classification
EMP_mask_array: numpy object to store the mask of EMP stars to convert it into observability
FI_SVM.dat: (permutation) feature importance for the used abundance ratios


### References ###
The file Saga_Jan2020.tsv is the recommended data set of the SAGA database:
http://sagadatabase.jp
-- Version December 11, 2019
-- Abundances are in units of [X/H] (including Li where we adopted A(Li)=3.26) normalized by the solar abundances of Asplund et al. (2009).
-- Recommended list of data, provided by one record for one star, following our priority parameter. See the SAGA paper series IV for the definition of the priority parameter.


### Using your own EMP abundances ###
Please see the file convert_user.py for further details.
You can add your new EMP stars to the file data/EMP_user.dat
One EMP star per line, column descriptions are provided in convert_user.py
The reference (last column of EMP_user.dat) should not contain spaces, i.e., python should be able to recognise it as one string
The values here should already include the carbon correction if desired. Please note, if your star appears already in our EMP comilation,
it can happen that the carbon correction is applied again.
You can then find the results in the file results/EMP_monomulti_NSNe5.dat
If you want to run only your EMP stars (and not the compilation from Hartwig+23), you can set
EMPset = "User-defined"
in the file parameters.py. This will speed up the calculation.


### Format of the file AllStarsFeH-3.0_NoOverlap.txt, which contains most of our EMP stars ###

column number:  contents
1:   starname
2 – 4:  [Fe/H] *1, error *2, flag *3
5 – 7: [C/Fe], error, flag
8 – 10: [N/Fe], error, flag
11 – 13: [O/Fe], error, flag
14 – 16: [Na/Fe], error, flag
17 – 19: [Mg/Fe], error, flag
20 – 22: [Al/Fe], error, flag
23 – 25: [Si/Fe], error, flag
26 – 28: [Ca/Fe], error, flag
29 – 31: [Sc/Fe], error, flag
32 – 34: [TiI/Fe], error, flag
35 – 37: [TiII/Fe], error, flag
38 – 40: [CrI/Fe], error, flag
41 – 43: [CrII/Fe], error, flag
44 – 46: [Mn/Fe], error, flag
47 – 49: [Co/Fe], error, flag
50 – 52: [Ni/Fe], error, flag
53 – 55: [Zn/Fe], error, flag
56 – 58: [Sr/Fe], error, flag
59 – 61: [Ba/Fe], error, flag
62: RA and DEC
63: Reference code   *4
64: Number of measured elements except for Fe, Sr, or Ba

*1  [Fe/H] or [X/Fe] = -9.99 for non-measurement

*2  error = 0.0 for non-measurement

*3  The meaning of the flag depends on reference. For all the references,
flag>0: measured value
flag=0: non-measurement
flag=-1: upper limit
flag=-2: lower limit

*4  Reference code
1. Yong, D., et al. 2013, ApJ, 762, 26
2. Cohen, J., et al. 2013, ApJ, 778, 56
3. Roederer, I. U., et al. 2014, ApJ, 147, 136
4. Jacobson, H. R., et al. 2015, ApJ, 807, 171
5. Hansen, T., et al. 2014, ApJ, 787, 162
6. Placco, V., et al. 2015, ApJ, 809, 136
7. Frebel, A., et al. 2015, ApJ, 810L, 27
8. Melendez, J., et al. 2016, A&A, 585L, 5
9. Placco, V., et al. 2016, ApJ, 833, 21
