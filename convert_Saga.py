#takes compilation from saga date base: "Text data in tsv format (without errors)"
#from http://sagadatabase.jp
#and coverts it into the right format for our mono/multi classification
import numpy as np
import glob
import csv
import io
from matplotlib import pyplot as plt
from parameters import *
from ConvertRatios import ConvertToRatios

def is_number(s):
    try:
        float(s)
        return True
    except ValueError:
        return False

def StripLimit(s):
    if ">" in s:
        s = s.replace(">","")
    if "<" in s:
        s = s.replace("<","")
    return s
    
def ConvertSaga(IonList,FeHdiv):
    XFe = 0.
    XFe_low = 0.
    XFe_up = 0.
    XFe_count = 0.
#    XFe_count_limit = 0.
    N_lower_limit = 0.
    N_upper_limit = 0.
#    UpperLimit = False
#    LowerLimit = False
    for ion in IonList:
        if(is_number(ion)):
            XFe += float(ion)
            XFe_count += 1.
        elif ">" in ion:
#            LowerLimit = True
            ion = ion.replace(">","")
            if(is_number(ion)):
                XFe_low += float(ion)
                N_lower_limit += 1.
        elif "<" in ion:
#            UpperLimit = True
            ion = ion.replace("<","")
            if(is_number(ion)):
                XFe_up += float(ion)
                N_upper_limit += 1.
#    if(UpperLimit and LowerLimit):
#        print("Upper and Lower Limit detected")#only once
    if (XFe_count+N_upper_limit+N_lower_limit == 0):
        #no data
        return -9.99, 0
    elif (XFe_count == 0):
        if (N_upper_limit > 0):
            return (XFe_up/N_upper_limit)-FeHdiv, -1
        elif (N_lower_limit > 0):
            return (XFe_low/N_lower_limit)-FeHdiv, -2
        else:
            return -9.99, 0
    else:
        return (XFe/XFe_count)-FeHdiv, 1




def EMP_saga():
    #get abundances and mask for observed EMP stars
    
    #output:
    mask_array = np.zeros(up_to_elem)
    obs_array = np.zeros(up_to_elem)
    names = []
    FeH = []
    FeH_err = []
    C_err = []
    BaFe = [] #to check for s-process enhancement
    EuFe = [] #to check for r-process enhancement
    
    #initialise empty lists
    Fe_list = []
    C_ion = []
    N = []
    O_ion = []
    Na_ion = []
    Mg_ion = []
    Al_ion = []
    Si_ion = []
    P = []
    S_ion = []
    K_ion = []
    Ca_ion = []
    Sc_ion = []
    Ti_ion = []
    V_ion = []
    Cr_ion = []
    Mn_ion = []
    Fe_ion = []
    Ni_ion = []
    Co_ion = []
    Cu_ion = []
    Zn_ion = []
    Ba_ion = []
    Eu_ion = []

    
    all_names = []
    Nsaga = 0
    print("Using 2020 file")
    Saga_file = 'data/Saga_Jan2020.tsv'
#    Saga_file = 'data/Saga_table_20190923.tsv'
#    Saga_file = 'data/BothSort_table_20190923.tsv'
    
    #data needs to be sorted according to "object" column so that same objects are next to each other
    with io.open(Saga_file,'rt',encoding="ISO-8859-1") as saga:
#        saga_in = csv.reader(codecs.iterdecode(saga,'utf-8'),delimiter='\t')
        saga_in = csv.reader(saga, delimiter='\t')
        for i, row in enumerate(saga_in):
            all_names = np.append(all_names,row[0])
            Nsaga += 1
    with io.open(Saga_file,'rt',encoding="ISO-8859-1") as saga:
        saga_in = csv.reader(saga, delimiter='\t')
        for i, row in enumerate(saga_in):
#            row = saga_in[i]
            if(i == 0):
                header = row
            else:
                #[Fe/H]
                name = row[0]
                FeH_now = 0.
                Fe_count = 0.
                Fe_list = np.append(Fe_list,[row[166],row[169],row[172]])#iron isotopes
                for FeH_saga in Fe_list:
                    FeH_saga = StripLimit(FeH_saga)
                    if(is_number(FeH_saga)):
                        FeH_now += float(FeH_saga)
                        Fe_count += 1.
                if (Fe_count == 0):
                    #No [Fe/H] measurement
                    Fe_list = []
#                    print("no Fe/H measurement")
                    continue
                FeH_now = FeH_now/Fe_count
                if (FeH_now > -3):
                    #metallicity too high
                    Fe_list = []
#                    print("Metallicity too high:",FeH_now)
                    continue
#                print(name,FeH_now)

                #extract data
                #(upper/lower limit 2 columns after measured value)
                #bring into same format as first paper
                C_ion = np.append(C_ion,[row[30]+row[28],row[39]+row[37]])#C2, CH?
                C_err1 = row[29]
                C_err2 = row[38]
                N_C = 0
                C_mean = 0.
                if(is_number(C_err1)):
                    C_mean += float(C_err1)**2
                    N_C += 1.
                if(is_number(C_err2)):
                    C_mean += float(C_err2)**2
                    N_C += 1.
                if(N_C>0):
                    C_err = np.append(C_err,np.sqrt(C_mean)/N_C)
                    
                N = np.append(N,row[42]+row[40])
                O_ion = np.append(O_ion,[row[51]+row[49],row[54]+row[52],row[57]+row[55]])
                Na_ion = np.append(Na_ion,[row[66]+row[64],row[69]+row[67]])
                Mg_ion = np.append(Mg_ion,[row[72]+row[70],row[75]+row[73],row[78]+row[76]])
                Al_ion = np.append(Al_ion,[row[81]+row[79],row[84]+row[82]])
                Si_ion = np.append(Si_ion,[row[87]+row[85],row[90]+row[88],row[93]+row[91]])
                P = np.append(P,row[96]+row[94])                
                S_ion = np.append(S_ion,[row[99]+row[97],row[102]+row[100],row[105]+row[103]])
                K_ion = np.append(K_ion,[row[108]+row[106],row[111]+row[109]])
                Ca_ion = np.append(Ca_ion,[row[114]+row[112],row[117]+row[115],row[120]+row[118]])
                Sc_ion = np.append(Sc_ion,[row[123]+row[121],row[126]+row[124],row[129]+row[127]])
                Ti_ion = np.append(Ti_ion,[row[132]+row[130],row[135]+row[133],row[138]+row[136]])                
                V_ion = np.append(V_ion,[row[141]+row[139],row[144]+row[142],row[147]+row[145]])
                Cr_ion = np.append(Cr_ion,[row[150]+row[148],row[153]+row[151],row[156]+row[154]])
                Mn_ion = np.append(Mn_ion,[row[159]+row[157],row[162]+row[160],row[165]+row[163]])

                Fe_ion = np.append(Fe_ion,[row[168]+row[166],row[171]+row[169],row[174]+row[172]])
                Fe_err1 = row[167]
                Fe_err2 = row[170]
                Fe_err3 = row[173]
                N_Fe = 0
                Fe_mean = 0.
                if(is_number(Fe_err1)):
                    Fe_mean += float(Fe_err1)**2
                    N_Fe += 1.
                if(is_number(Fe_err2)):
                    Fe_mean += float(Fe_err2)**2
                    N_Fe += 1.
                if(is_number(Fe_err3)):
                    Fe_mean += float(Fe_err3)**2
                    N_Fe += 1.
                if(N_Fe>0):
                    FeH_err = np.append(FeH_err,np.sqrt(Fe_mean)/N_Fe)
            
                Co_ion = np.append(Co_ion,[row[177]+row[175],row[180]+row[178],row[183]+row[181]])
                Ni_ion = np.append(Ni_ion,[row[186]+row[184],row[189]+row[187],row[192]+row[190]])
                Cu_ion = np.append(Cu_ion,[row[195]+row[193],row[198]+row[196],row[201]+row[199]])                
                Zn_ion = np.append(Zn_ion,[row[204]+row[202],row[207]+row[205],row[210]+row[208]])
                
                Ba_ion = np.append(Ba_ion,[row[318]+row[316],row[321]+row[319]])
#                Ba_limit = row[317]+row[320]
                Eu_ion = np.append(Eu_ion,[row[357]+row[355],row[360]+row[358],row[363]+row[361]])
#                Eu_limit = row[356]+row[359]+row[362]
                
                if(i+1 < Nsaga):
#                    next_row = saga_in[i+1]
#                    next_name = all_names[i+1]
                    if(name == all_names[i+1]):
#                        print("combine entries of same star")
#                        print(name,all_names[i+1])
                        continue

#                print("add new EMP star",C_ion)
#after collected yields from different lines   

                #new entries, to be attached
                mask_now = np.zeros(up_to_elem)
                obs_now = np.full(up_to_elem,-9.99)
                             
                obs_now[5], mask_now[5] = ConvertSaga(C_ion,FeH_now)
#                print(name,"C,Fe",obs_now[5],C_ion,FeH_now)#okay!?
                obs_now[6], mask_now[6] = ConvertSaga(N,FeH_now)
                obs_now[7], mask_now[7] = ConvertSaga(O_ion,FeH_now)
                obs_now[10], mask_now[10] = ConvertSaga(Na_ion,FeH_now)
                obs_now[11], mask_now[11] = ConvertSaga(Mg_ion,FeH_now)
                obs_now[12], mask_now[12] = ConvertSaga(Al_ion,FeH_now)
                obs_now[13], mask_now[13] = ConvertSaga(Si_ion,FeH_now)
                obs_now[14], mask_now[14] = ConvertSaga(P,FeH_now)
                obs_now[15], mask_now[15] = ConvertSaga(S_ion,FeH_now)
                obs_now[18], mask_now[18] = ConvertSaga(K_ion,FeH_now)
                obs_now[19], mask_now[19] = ConvertSaga(Ca_ion,FeH_now)
                obs_now[20], mask_now[20] = ConvertSaga(Sc_ion,FeH_now)
                obs_now[21], mask_now[21] = ConvertSaga(Ti_ion,FeH_now)
                obs_now[22], mask_now[22] = ConvertSaga(V_ion,FeH_now)
                obs_now[23], mask_now[23] = ConvertSaga(Cr_ion,FeH_now)
                obs_now[24], mask_now[24] = ConvertSaga(Mn_ion,FeH_now)
                obs_now[25], mask_now[25] = ConvertSaga(Fe_ion,FeH_now)
                obs_now[27], mask_now[27] = ConvertSaga(Ni_ion,FeH_now)
                obs_now[26], mask_now[26] = ConvertSaga(Co_ion,FeH_now)
                obs_now[28], mask_now[28] = ConvertSaga(Cu_ion,FeH_now)
                obs_now[29], mask_now[29] = ConvertSaga(Zn_ion,FeH_now)
                Ba1_now, Ba2_now = ConvertSaga(Ba_ion,FeH_now)
                Eu1_now, Eu2_now = ConvertSaga(Eu_ion,FeH_now)
                
#                print("FeH:",FeH_now,obs_now[25])#mostly zero

#                n_avail = np.max([mask_now[5],0])+np.max([mask_now[11],0])+np.max([mask_now[19],0])+np.max([mask_now[23],0]+np.max([mask_now[25],0]+np.max([mask_now[27],0])
#                if(np.max([mask_now[5],mask_now[11],mask_now[19],mask_now[25]])<1):
                count_avail = 0
                for m in mask_now:
                    if(m == 1):#element available
                        count_avail += 1
#                print(count_avail)
                if(count_avail > 2):#to form at least three ratios 10
                    FeH = np.append(FeH,FeH_now)
                    names = np.append(names,name)
                    mask_array = np.vstack((mask_array,mask_now))
                    obs_array = np.vstack((obs_array,obs_now))
                    if(Ba2_now > 0):
                        BaFe = np.append(BaFe,Ba1_now)
                    else:
                        BaFe = np.append(BaFe,-9.99)
                    if(Eu2_now > 0):
                        EuFe = np.append(EuFe,Eu1_now)
                    else:
                        EuFe = np.append(EuFe,-9.99)
#                else:
#                    print("Not enough observed elements. We don't use this EMP star:",count_avail,name)
                
                #re-initialise empty lists: ready for new EMP star
                Fe_list = []
                C_ion = []
                N = []
                O_ion = []
                Na_ion = []
                Mg_ion = []
                Al_ion = []
                Si_ion = []
                P = []
                S_ion = []
                K_ion = []
                Ca_ion = []
                Sc_ion = []
                Ti_ion = []
                V_ion = []
                Cr_ion = []
                Mn_ion = []
                Fe_ion = []
                Ni_ion = []
                Co_ion = []
                Cu_ion = []
                Zn_ion = []
                Ba_ion = []
                Eu_ion = []
#                for abc, de in zip(obs_now,mask_now):
#                    print(abc,de)

                #input: saga list, output: [X/Fe] & mask
#                for col,val_now in zip(header,row):
#                    print("TBD: fill new line")
#                    print(col,row_now)
#            print("append")
            #TBD: H=np.append(H,data[20])

    Nstars = len(FeH)
    print("Number of EMP stars in Saga 2020:",Nstars)
    #delete first row
    #TBD: some stars have zero observed elements, a lot have no observed iron?!
    mask_array = mask_array[1:]
    obs_array = obs_array[1:]
    names = names.astype('unicode')

#    print("mean(C_err):",np.mean(C_err))
#    print("median(C_err):",np.median(C_err))
    if False:
        plt.hist(FeH_err,bins=32)#,range=(-6,6),bins=32
        plt.xlabel("d[C/H]")
        plt.tight_layout()
        plt.savefig("plots/Histo_C_err.pdf")
        plt.close()
    
#    print("mean(FeH_err):",np.mean(FeH_err))
#    print("median(FeH_err):",np.median(FeH_err))
        plt.hist(FeH_err,bins=32)#,range=(-6,6),bins=32
        plt.xlabel("d[Fe/H]")
        plt.tight_layout()
        plt.savefig("plots/Histo_FeH_err.pdf")
        plt.close()
    
    file = open("data/mask_saga.dat",'w')
    for i in range(Nstars):
        #print(names[i],FeH[i])
        for j in range(34):
            file.write(str(int(mask_array[i,j]))+" ")
        file.write(' \n')        
    file.close()

    mask_array, obs_array = ConvertToRatios(mask_array,obs_array,Nstars)

    return mask_array, obs_array, Nstars, names, FeH, BaFe, EuFe
