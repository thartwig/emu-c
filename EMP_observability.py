#Aux function to convert mask array into observability fraction
import numpy as np

EMP_list = np.load("results/EMP_mask_array.npy",allow_pickle=True)
#[ratios,mask_array]
ratios = EMP_list[0]
mask_array = EMP_list[1]

N_ratios = len(ratios)
N_EMP = np.shape(mask_array)[0]
print("N_EMP:",N_EMP)

file = open("results/EMP_observability.dat",'w')
for i in range(N_ratios):
    N_avail = np.count_nonzero(mask_array[:,i] == 1)
    print(ratios[i],N_avail)
    file.write(ratios[i].astype('U7')+" "+str(N_avail/N_EMP)+" \n")
    

file.close()
