#takes user-defined compilation from file data/EMP_user.dat
#and coverts it into the right format for our mono/multi classification
#Format i.e. columns:
#name [C/H] [C/H]err [O/H] [O/H]err ... [Zn/H] [Zn/H]err [Ba/H] [Ba/H]err [Eu/H] [Eu/H]err reference
#13+2 elements: C, O, Na, Mg, Al, Si, Ca, Cr, Mn, Fe, Co, Ni, Zn + Ba, Eu
#if value not available: -99

import numpy as np
import glob
import csv
import io
from matplotlib import pyplot as plt
from parameters import *
from ConvertRatios import ConvertToRatios_user


def EMP_user():
    #get abundances and mask for observed EMP stars
    
    #output:
    mask_array = np.zeros(up_to_elem)
    obs_array = np.zeros(up_to_elem)
    err_array = np.zeros(up_to_elem)
    FeH = []
    
    
#These stars should be EMP stars, and already C-corrected
#If no star-specific errors are provided, we use our error matrix for observational&theoretical uncertainties
#...this means that the input data should be 1D LTE
    user_file = 'data/EMP_user.dat'
    header = np.genfromtxt(user_file,max_rows=1,dtype=str)
    names = np.genfromtxt(user_file,skip_header=1,dtype=str,usecols=0)
    XoverH = np.genfromtxt(user_file,skip_header=1,dtype=float,usecols=(1,3,5,7,9,11,13,15,17,19,21,23,25,27,29))
    XoverH_err = np.genfromtxt(user_file,skip_header=1,dtype=float,usecols=(2,4,6,8,10,12,14,16,18,20,22,24,26,28,30))
    FeH = XoverH[:,9]
    BaFe = XoverH[:,13] - FeH
    EuFe = XoverH[:,14] - FeH

    Nuser = np.shape(XoverH)[0]
    Ncolumns = np.shape(XoverH)[1]
    Nuser = len(names)
 

    mask = np.zeros([Nuser,Ncolumns])
    for i in range(Nuser):
        for j in range(Ncolumns):
            if(XoverH[i,j] > -90):
                mask[i,j] = 1#observable
                             

                

    print("Number of EMP stars in user-defined list:",Nuser)
    #names = names.astype('unicode')

    
    file = open("data/mask_user.dat",'w')
    for i in range(Nuser):
        #print(names[i],FeH[i])
        for j in range(Ncolumns):
            file.write(str(int(mask[i,j]))+" ")
        file.write(' \n')        
    file.close()


    mask_array, obs_array = ConvertToRatios_user(mask,XoverH,Nuser)

    return mask_array, obs_array, XoverH_err, Nuser, names, FeH, BaFe, EuFe
