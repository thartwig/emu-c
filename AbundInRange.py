import numpy as np
import time
from parameters import up_to_elem, MinMaxAvail, ratios, ElemSaved

#function to check the abundance range that EMP stars span
def InRange(N_EMP,EMP_obs):
    EMPinMinMax = np.full([N_EMP,up_to_elem+1],1)#avail: 1 not avail: 0 (last entry cummulative)
    for i in range(N_EMP):
        for j in range(up_to_elem):
#            if(j<=5 or j==20 or j==21 or j==25):# or j>=23):
#                continue
            if (EMP_obs[i,j]==-9.99):#data not available
                continue
            if(MinMaxAvail[j,0] > EMP_obs[i,j]):
                EMPinMinMax[i,j] = 0
                EMPinMinMax[i,up_to_elem] = 0
#                print(i,"not avail 0:",ratios[j],MinMaxAvail[j,0],EMP_obs[i,j])
#                break
            elif(MinMaxAvail[j,1] < EMP_obs[i,j]):#Cr causes problems?! also Mn, all iron peak elements?
                EMPinMinMax[i,j] = 0
                EMPinMinMax[i,up_to_elem] = 0
#                print(i,"not avail 1:",ratios[j],MinMaxAvail[j,1],EMP_obs[i,j])
#                break
#        print("FINAL:",i,EMPinMinMax[i])
    return EMPinMinMax


#previous function did many 1D tests
#this optional function tries to obtain the same information in higher dimensions
#computationally very expensive in higher dimensions
def ExtraP(p,cloud,iTree,elemUsed):
    global ElemSaved    
    #test if point p is outside of scatterpoints in clouds
    #calculates mean distance of points in clouds
    #checks how far p 
    from scipy.spatial.distance import pdist, cdist
    
    i_elem = -99
    for i in range(len(ElemSaved)):
        if (np.array_equal(elemUsed,ElemSaved[i])):
            i_elem = i
            break
        
    #test only for a fraction of elements
    Nuse = int(cloud.shape[0]/32)
    if(Nuse<10):#Nuse too small
        Nuse = int(cloud.shape[0])
        
    #use saved hull
    if(i_elem > -99):
#                print("use saved hull")
            meanDist1 = MaxHullSaved[i_elem]
    else:
#        print("calculate cloud distances and save it",elemUsed)
        Y1 = pdist(cloud[0:Nuse,:],'sqeuclidean')#sqeuclidean faster than euclidean
        meanDist1 = np.mean(Y1)
        #hull = Delaunay(hull,qhull_options="Q0/Qx")
        MaxHullSaved.append(meanDist1)
        ElemSaved.append(elemUsed)

    p2D = np.reshape(p, (1,len(p)))#convert to 2D
    Y2 = cdist(p2D,cloud[0:Nuse,:],'sqeuclidean')
    meanDist2 = np.mean(Y2)
    ExtraPratio = meanDist2/meanDist1

    return ExtraPratio

#this optinal function calculates the hull of the training yields and checks if EMP yields are inside or outside
#computationally very expensive in higher dimensions
def in_hull(p, hull,iTree,elemUsed,MaxHullSaved,ElemSaved):
    """
    Test if points in `p` are in `hull`

    `p` should be a `NxK` coordinates of `N` points in `K` dimensions
    `hull` is either a scipy.spatial.Delaunay object or the `MxK` array of the 
    coordinates of `M` points in `K`dimensions for which Delaunay triangulation
    will be computed
    """
    from scipy.spatial import Delaunay
    
    if not isinstance(hull,Delaunay):
    
        i_elem = -99
        for i in range(len(ElemSaved)):
            if (np.array_equal(elemUsed,ElemSaved[i])):
                i_elem = i
                break
            #use saved hull
        if(i_elem > -99):
#                print("use saved hull")
                hull = MaxHullSaved[i_elem]
        else:
            print("calculate hull and save it",elemUsed)
            hull = Delaunay(hull,qhull_options="Q0/Qx")
            #           Fx     - extreme points (convex hull vertices)
            #Qm     Only  process  points  that would otherwise increase max_outside.  Other points are treated as coplanar or interior points.
            #Q0 Turn off pre-merging as a default option.  With Q0/Qx and without explicit pre merge options, Qhull ignores precision issues while
            #constructing the  convex  hull. This may lead to precision errors.  If so, a descriptive warning is generated.
            #Q2     With Q2, Qhull merges all facets at once instead of  using  independent  sets  of merges and then retesting.
            #Q6     With Q6, Qhull does not pre-merge concave or coplanar facets.
            MaxHullSaved.append(hull)
            ElemSaved.append(elemUsed)
#        hull = Delaunay(hull)
        #Qm - only process points that increase the maximum outer plane
#        print(len(MaxHullSaved),len(ElemSaved))
#        print("saved",ElemSaved)

    return (hull.find_simplex(p)>=0),MaxHullSaved,ElemSaved


def InHull(EMP_obs,train,iTree,elemUsed,MaxHullSaved,ElemSaved):
#    print("Test if abundances are wihtin convex hull")
#    EMPinMinMax = np.full([N_EMP,up_to_elem+1],1)#avail: 1 not avail: 0 (last entry cummulative)
    
#    EMP_obs = ReduceDimensions(EMP_obs)
#    train = ReduceDimensions(train)
    
    tested = EMP_obs
    cloud  = train

#    ih = in_hull(tested,cloud)
#    print("In hull:",np.count_nonzero(ih == False))
#    print("Out hull:",np.count_nonzero(ih == True))
    
    return in_hull(tested,cloud,iTree,elemUsed,MaxHullSaved,ElemSaved)    

#from stackexchange - not used
def plot_in_hull(p, hull):
    """
    plot relative to `in_hull` for 2d data
    """
    import matplotlib.pyplot as plt
    from matplotlib.collections import PolyCollection, LineCollection

    from scipy.spatial import Delaunay
    if not isinstance(hull,Delaunay):
        hull = Delaunay(hull)

    # plot triangulation
    poly = PolyCollection(hull.points[hull.vertices], facecolors='w', edgecolors='b')
    plt.clf()
    plt.title('in hull')
    plt.gca().add_collection(poly)
    plt.plot(hull.points[:,0], hull.points[:,1], 'o', hold=1)


    # plot the convex hull
    edges = set()
    edge_points = []

    def add_edge(i, j):
        """Add a line between the i-th and j-th points, if not in the list already"""
        if (i, j) in edges or (j, i) in edges:
            # already added
            return
        edges.add( (i, j) )
        edge_points.append(hull.points[ [i, j] ])

    for ia, ib in hull.convex_hull:
        add_edge(ia, ib)

    lines = LineCollection(edge_points, color='g')
    plt.gca().add_collection(lines)
    plt.show()    

    # plot tested points `p` - black are inside hull, red outside
    inside = in_hull(p,hull)
    plt.plot(p[ inside,0],p[ inside,1],'.k')
    plt.plot(p[-inside,0],p[-inside,1],'.r')



def ReduceDimensions(SN_array,mask_array,test_array,elem_used):
#can be used to spped up previous functions
#    print("Start reducing dimensions",elem_used)
#    start = time.time()
    w = test_array
    x = SN_array
    y = mask_array
    z = ratios#[0:up_to_elem]#elem_names_temp
#rows to delete
    delete_index=[]
    for i in range(x.shape[1]):
        if (z[i] not in elem_used):
        #element not used
            delete_index.append(i)
            continue
        elif (np.min(x[:,i]) == np.max(x[:,i])):
        #has zero size
            delete_index.append(i)
            continue
        elif (y[i]<=0):
        #element not available
            delete_index.append(i)
            continue
#    print(delete_index)
    w = np.delete(w,delete_index,axis=0)
    x = np.delete(x,delete_index,axis=1)
    y = np.delete(y,delete_index,axis=0)
    z = np.delete(z,delete_index,axis=0)

    if(DoHull):
        while(x.shape[1]>=8):# hull calculation crashes for too high dimensions. Plus, this case is rare anyways
        #index 4 to not always delete the same elements
            w = np.delete(w,4,axis=0)
            x = np.delete(x,4,axis=1)
            y = np.delete(y,4,axis=0)
            z = np.delete(z,4,axis=0)
#            print("Reduced dimensions to",x.shape[1])
        
#    print("ReduceDim time:",time.time() - start)
#    print("End reducing dimensions. final elements:",x.shape[1],z) 
    return x,w,z
