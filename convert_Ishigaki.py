import numpy as np
import glob
import csv
from numpy import genfromtxt
from parameters import *
from ConvertRatios import ConvertToRatios


def constructmiho():
#check which SN models have a small enough chi2 so that they are realistic enough to be used
    my_data = genfromtxt("data/minchi2_UniqueYields.dat", delimiter='\t',skip_header=1)
    MyChi = my_data[:,4]
    Chi2Min = my_data[:,4]
    Chi2Mean = my_data[:,6]
    Chi2Median = my_data[:,7]
    MyMask = (Chi2Mean < 15.1)#Mean

    chi = MyChi[MyMask]
    mass = list(my_data[MyMask,0])
    energy = list(my_data[MyMask,1])
    Mmass = list(my_data[MyMask,2])
    f_ej = list(my_data[MyMask,3])
    #converted to lists to make appending easier

    print("Selected yields before:",len(mass))
    if True:
        #augment with best fitting yields of individual EMP stars from Ishigaki+18
        #even if their Chi2Mean is not selected
        I18_BestFit = np.genfromtxt('data/Ishigaki18_bestparams_summary.txt',usecols=(3,4,5,6),delimiter=',',skip_header=1)
        #Mass,Energy,Mmix,fej
        N_I18 = np.shape(I18_BestFit)[0]
        print("Lines from Miho:",N_I18)
        for i in range(N_I18):
            #check if i-th best fit yield is already selected
            i_already = False
            for j in range(len(mass)):
                if(I18_BestFit[i,0] == mass[j] and
                   I18_BestFit[i,1] == energy[j] and
                   I18_BestFit[i,2] == Mmass[j] and
                   I18_BestFit[i,3] == f_ej[j]):
                    i_already = True
                    break
            if(not i_already):
                mass.append(I18_BestFit[i,0])
                energy.append(I18_BestFit[i,1])
                Mmass.append(I18_BestFit[i,2])
                f_ej.append(I18_BestFit[i,3])

    print("Selected yields after augmentation:",len(mass))
    print("Converting Yields...")
    file = open("data/Ishigaki18.dat",'w')
    file.write('#    H to Ge in Msun \n')


    for M,E,Mmix,fej in zip(mass,energy,Mmass,f_ej):
#        print(M,E,Mmix,fej)
        file_string = "data/yield_mass_miho/*"+str(int(M))+"*"+str(E)+"*Mout"+str(Mmix)+"*"+str(fej)+"*"+".dat"
#        print(file_string)
#find corresponding file
        listing = glob.glob(file_string)
        nSNe = len(listing)
        if(nSNe != 1):
            print("PROBLEM: Number SN files:",nSNe)
            print(M,E,Mmix,fej)
            print(listing)

        for f in listing:
#            print(f)
            with open(f,'r') as fh:
                for curline in fh:
                 # exclude first, last, and C+N lines
                    if(not(curline.startswith("#")) and
                       not(curline.startswith(" Ni56")) and
                       not(curline.startswith("   CN"))):
                        file.write(curline[83:93])
                        file.write(' ')
            file.write('\n')
        
    file.close()

def mask_miho():
    #construct mask from compilation of observed stars
    #available: 1
    #not available: 0
    #upper limit: -1


    file = open("data/mask_miho.dat",'w')
    file.write('#    H to Ge: available: >=1, not available: 0, upper limit: -1 \n')
    
#    data_avail = np.genfromtxt('data/AllStarsFeH-3.0_NoOverlap.txt',usecols=(1,4,7,10,13,16,19,22,25,28,31,34,37,40,43,46,49,52,55,58),delimiter=',')
    data_upperLimit = np.genfromtxt('data/AllStarsFeH-3.0_NoOverlap.txt',usecols=(3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60),delimiter=',',dtype=int).T
    #number of lines: -1 if upper limit, 0 if not avail
    #for: Fe C N O Na Mg Al Si Ca Sc TiI TiII CrI CrII Mn Co Ni Zn Sr Ba
    mask_Fe = data_upperLimit[0]
    mask_C = data_upperLimit[1]
    mask_N = data_upperLimit[2]
    mask_O = data_upperLimit[3]
    mask_Na = data_upperLimit[4]
    mask_Mg = data_upperLimit[5]
    mask_Al = data_upperLimit[6]
    mask_Si = data_upperLimit[7]
    mask_Ca = data_upperLimit[8]
    mask_Sc = data_upperLimit[9]
    mask_TiI = data_upperLimit[10]
    mask_TiII = data_upperLimit[11]
    mask_CrI = data_upperLimit[12]
    mask_CrII = data_upperLimit[13]
    mask_Mn = data_upperLimit[14]
    mask_Co = data_upperLimit[15]
    mask_Ni = data_upperLimit[16]
    mask_Zn = data_upperLimit[17]
    mask_Sr = data_upperLimit[18]
    mask_Ba = data_upperLimit[19]

    print("Constructing mask from", len(mask_C)," compiled EMP stars...")
    Nstars = len(mask_C)
    mask_array = np.zeros([Nstars,up_to_elem+2],dtype=int)

    #exclude H, He, Li, Be, B
    mask_array[:,5] = mask_C
    mask_array[:,6] = mask_N
    mask_array[:,7] = mask_O
    mask_array[:,8] = np.zeros(Nstars,dtype=int)
    mask_array[:,9] = np.zeros(Nstars,dtype=int)
    mask_array[:,10] = mask_Na
    mask_array[:,11] = mask_Mg
    mask_array[:,12] = mask_Al
    mask_array[:,13] = mask_Si
    mask_array[:,14] = np.zeros(Nstars,dtype=int)#P
    mask_array[:,15] = prob_mask(Nstars,Nstars*0/352,Nstars*1/352)#S: 1/352 upper limit
    mask_array[:,16] = np.zeros(Nstars,dtype=int)#Cl
    mask_array[:,17] = np.zeros(Nstars,dtype=int)#Ar
    mask_array[:,18] = prob_mask(Nstars,Nstars*40/352,Nstars*2/352)#K: 40/352 measured, 2/352 upper limit
    mask_array[:,19] = mask_Ca
    mask_array[:,20] = mask_Sc
    mask_array[:,21] = np.maximum.reduce([mask_TiI,mask_TiII])
    mask_array[:,22] = prob_mask(Nstars,Nstars*72/352,Nstars*60/352)#V: 72/352 measured, 60/352 upper limit
    mask_array[:,23] = np.maximum.reduce([mask_CrI,mask_CrII])
    mask_array[:,24] = mask_Mn
    mask_array[:,25] = mask_Fe
    mask_array[:,26] = mask_Co
    mask_array[:,27] = mask_Ni
    mask_array[:,28] = prob_mask(Nstars,Nstars*6/352,Nstars*21/352)#Cu: 6/352 measured, 21/352 upper limit
    mask_array[:,29] = mask_Zn
    mask_array[:,30] = prob_mask(Nstars,Nstars*0/352,Nstars*15/352)#Ga: 15/352 upper limit
    mask_array[:,31] = prob_mask(Nstars,Nstars*0/352,Nstars*1/352)#Ge: 1/352 upper limit
    mask_array[:,32] = mask_Sr
    mask_array[:,33] = mask_Ba



    for i in range(Nstars):
        for j in range(up_to_elem+2):
            file.write(str(mask_array[i,j]))
            file.write(' ')
        file.write('\n')
        
    file.close()

def prob_mask(Nstars,Nmeasured,Nupper):
    #create mask based on JINA probabilities
    p_mask = np.zeros(Nstars,dtype=int)
    if (Nmeasured == 0 and Nupper == 0):
        return p_mask
    Nmeasured = int(np.round(Nmeasured))
    Nupper = int(np.round(Nupper))
    p_mask[0:Nmeasured] = 1
    p_mask[Nmeasured:Nmeasured+Nupper] = -1
    np.random.shuffle(p_mask)
    return p_mask


def EMP_miho():
    #get abundances and mask for observed EMP stars

    data_Names = np.genfromtxt('data/AllStarsFeH-3.0_NoOverlap.txt',usecols=(0,1),delimiter=',',dtype="S").T    
    data_avail = np.genfromtxt('data/AllStarsFeH-3.0_NoOverlap.txt',usecols=(1,4,7,10,13,16,19,22,25,28,31,34,37,40,43,46,49,52,55,58),delimiter=',').T
    data_upperLimit = np.genfromtxt('data/AllStarsFeH-3.0_NoOverlap.txt',usecols=(3,6,9,12,15,18,21,24,27,30,33,36,39,42,45,48,51,54,57,60),delimiter=',',dtype=int).T
    
    names = data_Names[0].astype('unicode')
    FeH = data_Names[1].astype(np.float)
    
    #number of lines: -1 if upper limit, 0 if not avail
    #for: Fe C N O Na Mg Al Si Ca Sc TiI TiII CrI CrII Mn Co Ni Zn Sr Ba
    mask_Fe = data_upperLimit[0]
    mask_C = data_upperLimit[1]
    mask_N = data_upperLimit[2]
    mask_O = data_upperLimit[3]
    mask_Na = data_upperLimit[4]
    mask_Mg = data_upperLimit[5]
    mask_Al = data_upperLimit[6]
    mask_Si = data_upperLimit[7]
    mask_Ca = data_upperLimit[8]
    mask_Sc = data_upperLimit[9]
    mask_TiI = data_upperLimit[10]
    mask_TiII = data_upperLimit[11]
    mask_CrI = data_upperLimit[12]
    mask_CrII = data_upperLimit[13]
    mask_Mn = data_upperLimit[14]
    mask_Co = data_upperLimit[15]
    mask_Ni = data_upperLimit[16]
    mask_Zn = data_upperLimit[17]
    mask_Sr = data_upperLimit[18]
    mask_Ba = data_upperLimit[19]
    
    Nstars = len(mask_C)
    mask_array = np.zeros([Nstars,up_to_elem],dtype=int)
    
    mask_array[:,5] = mask_C
    mask_array[:,6] = mask_N
    mask_array[:,7] = mask_O
    mask_array[:,8] = np.zeros(Nstars,dtype=int)
    mask_array[:,9] = np.zeros(Nstars,dtype=int)
    mask_array[:,10] = mask_Na
    mask_array[:,11] = mask_Mg
    mask_array[:,12] = mask_Al
    mask_array[:,13] = mask_Si
    mask_array[:,14] = np.zeros(Nstars,dtype=int)#P
    mask_array[:,15] = np.zeros(Nstars,dtype=int)
    mask_array[:,16] = np.zeros(Nstars,dtype=int)#Cl
    mask_array[:,17] = np.zeros(Nstars,dtype=int)#Ar
    mask_array[:,18] = np.zeros(Nstars,dtype=int)
    mask_array[:,19] = mask_Ca
    mask_array[:,20] = mask_Sc
    mask_array[:,21] = np.maximum.reduce([mask_TiI,mask_TiII])
    mask_array[:,22] = np.zeros(Nstars,dtype=int)
    mask_array[:,23] = np.maximum.reduce([mask_CrI,mask_CrII])
    mask_array[:,24] = mask_Mn
    mask_array[:,25] = mask_Fe
    mask_array[:,26] = mask_Co
    mask_array[:,27] = mask_Ni
    mask_array[:,28] = np.zeros(Nstars,dtype=int)
    mask_array[:,29] = mask_Zn
#   mask_array[:,30] = np.zeros(Nstars,dtype=int)
 #   mask_array[:,31] = np.zeros(Nstars,dtype=int)
#    mask_array[:,32] = mask_Sr
#    mask_array[:,33] = mask_Ba

#Iron: [Fe/H]
#other elements: [X/Fe]?
    obs_Fe = data_avail[0]
    obs_C = data_avail[1]
    obs_N = data_avail[2]
    obs_O = data_avail[3]
    obs_Na = data_avail[4]
    obs_Mg = data_avail[5]
    obs_Al = data_avail[6]
    obs_Si = data_avail[7]
    obs_Ca = data_avail[8]
    obs_Sc = data_avail[9]
    obs_TiI = data_avail[10]
    obs_TiII = data_avail[11]
    obs_CrI = data_avail[12]
    obs_CrII = data_avail[13]
    obs_Mn = data_avail[14]
    obs_Co = data_avail[15]
    obs_Ni = data_avail[16]
    obs_Zn = data_avail[17]
#    obs_Sr = data_avail[18]
#    obs_Ba = data_avail[19]

    obs_array = np.full([Nstars,up_to_elem],-9.99)

#convert all to [X/H]
    obs_array[:,5] = obs_C+obs_Fe
    obs_array[:,6] = obs_N+obs_Fe
    obs_array[:,7] = obs_O+obs_Fe
    obs_array[:,10] = obs_Na+obs_Fe
    obs_array[:,11] = obs_Mg+obs_Fe
    obs_array[:,12] = obs_Al+obs_Fe
    obs_array[:,13] = obs_Si+obs_Fe
    obs_array[:,19] = obs_Ca+obs_Fe
    obs_array[:,20] = obs_Sc+obs_Fe
    obs_array[:,21] = np.maximum.reduce([obs_TiI,obs_TiII])+obs_Fe

    obs_array[:,23] = np.maximum.reduce([obs_CrI,obs_CrII])+obs_Fe
    obs_array[:,24] = obs_Mn+obs_Fe
    obs_array[:,25] = obs_Fe
    obs_array[:,26] = obs_Co+obs_Fe
    obs_array[:,27] = obs_Ni+obs_Fe
    obs_array[:,29] = obs_Zn+obs_Fe

    mask_array, obs_array = ConvertToRatios(mask_array,obs_array,Nstars)

    return mask_array, obs_array, Nstars, names, FeH

"""
### REFERENCES ###
Name	Ref
SDSSJ0815+4729	Aguado18
SMSSJ1605-1443	Nordlander19
53436-1996-093	Yong13
HE0130-2303	Yong13
BS16084-160	Yong13
HD4306		Yong13
BS16080-093	Yong13
BS16085-050	Yong13
CS22965-054	Yong13
CS29497-034	Yong13
HD88609		Roederer14
HD200654	Roederer14
CS22960-010	Roederer14
HD126587	Roederer14
CS22880-086	Roederer14
CD-241782	Roederer14
CS22953-003	Roederer14
CS22166-016	Roederer14
CS31082-001	Roederer14
CS22958-083	Roederer14
HD237846	Roederer14
CS22944-032	Roederer14
BD-206008	Roederer14
BD-185550	Roederer14
CS22185-007	Roederer14
CS30314-067	Roederer14
BS16545-089	Cohen13
HE1338-0052	Cohen13
"""
