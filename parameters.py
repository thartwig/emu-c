import numpy as np
#augmenting mock data size by factor 2^expo
expo = 6

#SVM Dimension
Ndim = 4

#enrichment by up to how many SNe shall be tested? >=2
NSNe= 5
L_NSN = NSNe-1#Loop size for NSN


N_rand = 49 #number of random samplings for bootstrap #odd number to break ties
#if set to 1, no re-sampling is performed and std=0


#Ntot examples
ftrain = 0.5
#two sets for cross validation
fCV1 = 0.1
fCV2 = 0.3
#percentage of blind test data: 1-fCV1-fCV2


#Which set of theoretical SN yields?
SNset = "Miho18"
#SNset = "HW10"

#Which set of observed EMP stars?
#EMPset = "Miho18"
#EMPset = "Saga19"
EMPset = "MihoSaga"
#EMPset = "User-defined"

ElemSaved = []

elements = np.array(['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P','S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe'])
elements_all = np.array(['H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P','S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn','Ga','Ge','As','Se','Br','Kr','Rb','Sr','Y','Zr','Nb','Mo','Tc','Ru','Rh','Pd','Ag','Cd','In','Sn','Sb','Te','I','Xe','Cs','Ba','La','Ce','Pr','Nd','Pm','Sm','Eu','Gd','Tb','Dy','Ho','Er','Tm','Yb','Lu','Hf','Ta','W','Re','Os','Ir','Pt','Au','Hg','Tl','Pb','Bi','Th','U'])

#elements used
#          1   2    3    4    5   6   7   8   9   10   11   12   13   14   15  16  17   18   19  20   21   22   23  24   25   26   27   28   29   30
#elements 'H','He','Li','Be','B','C','N','O','F','Ne','Na','Mg','Al','Si','P','S','Cl','Ar','K','Ca','Sc','Ti','V','Cr','Mn','Fe','Co','Ni','Cu','Zn'
#idx_use = [6,7,8,11,12,13,14,15,16,19,20,21,22,23,24,25,26,27,28,29,30]
idx_use = [6,8,11,12,13,14,20,24,25,26,27,28,30]#no Cu
#idx_use = [6,8,12,14,20,24,25,26,27,28,30]#no Cu, Al, Na
N_use = len(idx_use)
N_ratios = int(N_use*(N_use-1)/2)
print("Different elements used:",N_use,"   Different abundance ratios used:",N_ratios)

ratios = np.full(N_ratios,"0000000",dtype='S7')
ind = 0
for i in range(N_use):
    for j in range(i):
        ratios[ind] = "["+elements_all[idx_use[j]-1]+"/"+elements_all[idx_use[i]-1]+"]"
        ind += 1
#use abundances up to which element?
up_to_elem = N_ratios#N*(N-1)/2


MinMaxAvail = np.zeros([up_to_elem,2])#min and max available abundances

#solar abundances relative to iron
asplund = [-4.500,-3.430,6.450,6.120,4.800,-0.930,-0.330,-1.190,2.940,-0.430,1.260,-0.100,1.050,-0.010,2.090,0.380,2.000,1.100,2.470,1.160,4.350,2.550,3.570,1.860,2.070,0.000]


#-log10(X/H)
#hydrogen to Bi; +Th; +U
asplund_all = [0.000,1.070,10.950,10.620,9.300,3.570,4.170,3.310,7.440,4.070,5.760,4.400,5.550,4.490,6.590,4.880,6.500,5.600,6.970,5.660,8.850,7.050,8.070,6.360,6.570,4.500,7.010,5.780,7.810,7.440,8.960,8.350,9.700,8.660,9.460,8.750,9.480,9.130,9.790,9.420,10.540,10.120,12.000,10.250,11.090,10.430,11.060,10.290,11.200,9.960,10.990,9.820,10.450,9.760,10.920,9.820,10.900,10.420,11.280,10.580,12.000,11.040,11.480,10.930,11.700,10.900,11.520,11.080,11.900,11.160,11.900,11.150,12.120,11.150,11.740,10.600,10.620,10.380,11.080,10.830,11.100,10.250,11.350,11.980,12.540]

#mean isotope masses H to Kr
IsotopeMass=[1.01,4.00,6.94,9.01,10.8,12.0,14.0,16.0,19.0,20.2,23.0,24.3,27.0,28.1,30.1,32.1,35.5,40.0,39.1,40.1,45.0,47.9,50.9,52.0,55.0,55.8,58.9,63.5,65.4,69.7,72.6,74.9,79.0,79.9,84.8]
